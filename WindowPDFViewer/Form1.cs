﻿
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;

namespace WindowPDFViewer
{
    public partial class Form1 : Form
    {
        //declare
        private OpenFileDialog openFileDialog1;
        private OpenFileDialog openFileDialog2;
        private SaveFileDialog saveFileDialog1;
        private FontDialog fontDialog1;
        private string PDFText;
        private string path;
        private string mergePath;
        private string savePath;
        private string[] dataArray;
        private int selectedPage = 1;
        private bool firstLoad = false;
        private int controlPrintPage = 0;
        private string tempString;
        private int pointerIndex = 0;
        private int previousPointerIndex = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Pdf files (*.pdf)|*.pdf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Reset RTB fontstyle
                //rtbContent.ClearAllFormatting(new System.Drawing.Font("Microsoft Sans Serif", 8.25f));
                System.Drawing.Font newFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25f);
                rtbContent.Font = newFont;

                //prevent selectedindexchange first time error
                firstLoad = false;
                path = openFileDialog1.FileName.ToString();
                PDFText = itextsharp.pdfTextByPageNumber(path, 1);
                //assign combobox value
                cmbPage.Items.Clear();
                for (int indexA = 1; indexA <= itextsharp.getNumberOfPages(path); indexA++)
                {
                    cmbPage.Items.Add(indexA);
                }
                selectedPage = 1;
                cmbPage.SelectedIndex = 0;
                refreshTempReplace();
                //assign all data array
                dataArray = itextsharp.pdfArray(path);

                //assign textbox value
                txtPath.Text = path;
                rtbContent.Text = PDFText;
            }
            openFileDialog1.Dispose();
            Refresh();
        }

        private void cmbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (firstLoad != false)
            {
                refreshTempReplace();
                dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                PDFText = dataArray[int.Parse(cmbPage.SelectedItem.ToString()) - 1].ToString();
                rtbContent.Text = PDFText;
                selectedPage = int.Parse(cmbPage.SelectedItem.ToString());
            }
            firstLoad = true;           
        }
        

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                if (rtbContent.Text != null)
                {
                    saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Pdf files (*.pdf)|*.pdf";
                    saveFileDialog1.DefaultExt = "pdf";
                    saveFileDialog1.AddExtension = true;
                    saveFileDialog1.ShowDialog();

                    //save path allocate
                    if (saveFileDialog1.FileName != "")
                    {  
                        //font setting
                        FontFactory.RegisterDirectories();
                        System.Drawing.Font currentFont = rtbContent.SelectionFont;
                        iTextSharp.text.Font ifont = FontFactory.GetFont(currentFont.Name, currentFont.Size, BaseColor.BLACK);
                        itextsharp.pdfFont = ifont;

                        dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                        savePath = saveFileDialog1.FileName.ToString();
                        //remove empty page
                        dataArray = Features.removeEmptyPage(dataArray);
                        itextsharp.savePdfFile(savePath, dataArray.Length, dataArray, chkFooter.Checked);
                        MessageBox.Show("File save in: " + savePath);
                    }
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rtbContent.Text != null)
                {
                    if (txtPath.Text != null && txtPath.Text != "")
                    {
                        //font setting
                        FontFactory.RegisterDirectories();
                        System.Drawing.Font currentFont = rtbContent.SelectionFont;
                        iTextSharp.text.Font ifont = FontFactory.GetFont(currentFont.Name, currentFont.Size, BaseColor.BLACK);
                        itextsharp.pdfFont = ifont;

                        dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                        dataArray = Features.removeEmptyPage(dataArray);
                        itextsharp.savePdfFile(path, dataArray.Length, dataArray, chkFooter.Checked);
                        MessageBox.Show("File save in: " + path);

                    }
                    else
                    {
                        btnSaveAs_Click(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (cmbPage.Items.Count>=1)
            {
                refreshTempReplace();
                dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                firstLoad = false;
                dataArray = Features.addPage(dataArray);
                cmbPage.Items.Add((cmbPage.Items.Count + 1));
                selectedPage = cmbPage.Items.Count;
                cmbPage.SelectedIndex = cmbPage.Items.Count - 1;
                rtbContent.Clear();
            }
            else
            {
                MessageBox.Show("No record was chooses");
            }
            
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            controlPrintPage = 0;
            PrintDialog printDialog = new PrintDialog();
            PrintDocument documentToPrint = new PrintDocument();
            printDialog.Document = documentToPrint;

            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                documentToPrint.PrintPage += new PrintPageEventHandler(DocumentToPrint_PrintPage);
                documentToPrint.Print();
                MessageBox.Show("Page printed");
            }
        }

        private void DocumentToPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            
            float LinesPerPage = 0;
            float YPosition = 0;
            int Count = 0;
            float LeftMargin = e.MarginBounds.Left;
            float TopMargin = e.MarginBounds.Top;
            string Line = null;
            System.Drawing.Font PrintFont = this.rtbContent.Font;
            SolidBrush PrintBrush = new SolidBrush(Color.Black);

            LinesPerPage = e.MarginBounds.Height / PrintFont.GetHeight(e.Graphics);
           
                StringReader reader = new StringReader(dataArray[controlPrintPage]);
                while (Count < LinesPerPage && ((Line = reader.ReadLine()) != null))
                {
                    YPosition = TopMargin + (Count * PrintFont.GetHeight(e.Graphics));
                    e.Graphics.DrawString(Line, PrintFont, PrintBrush, LeftMargin, YPosition, new StringFormat());
                    Count++;
                }      

            if (controlPrintPage < dataArray.Length-1)
            {
                controlPrintPage++;
                e.HasMorePages = true;
            }
            else
            {
                e.HasMorePages = false;
            }
            PrintBrush.Dispose();      
        }

        private void btnFont_Click(object sender, EventArgs e)
        {
            fontDialog1 = new FontDialog();
            fontDialog1.Font = rtbContent.Font;

            if (fontDialog1.ShowDialog() == DialogResult.OK & !String.IsNullOrEmpty(rtbContent.Text))
            {
                rtbContent.Font = fontDialog1.Font;
                Features.fontName = fontDialog1.Font.Name;
                Features.fontSize = fontDialog1.Font.Size;
                Features.fontBold = fontDialog1.Font.Bold;
                Features.fontItalic = fontDialog1.Font.Italic;
                //Features.fontUnderlind = fontDialog1.Font.Underline;
            }
        
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {          
            try
            {
                if (txtSearch.Text != null)
                {
                    rtbContent.SelectAll();
                    rtbContent.SelectionBackColor = System.Drawing.Color.White;
                    rtbContent.DeselectAll();
                    int start = 0;
                    int end = rtbContent.Text.LastIndexOf(txtSearch.Text);
                    int searchFound = 0;

                    while (start < end)
                    {
                        int found = rtbContent.Find(txtSearch.Text, start, rtbContent.TextLength, RichTextBoxFinds.MatchCase);
                        rtbContent.SelectionBackColor = Color.Yellow;
                        start = rtbContent.Text.IndexOf(txtSearch.Text, start) + 1;
                        if (found >= 0)
                        {
                            searchFound++;
                        }
                    }

                    if(searchFound == 0)
                        MessageBox.Show("No record was found.", "Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else if(searchFound > 0)
                        MessageBox.Show("Found Record: "+searchFound.ToString(), "Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(string.IsNullOrEmpty(txtSearch.Text))
                {
                    MessageBox.Show("Search field cannot empty.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
 
        private void btnClear_Click(object sender, EventArgs e)
        {
            rtbContent.SelectAll();
            rtbContent.SelectionBackColor = System.Drawing.Color.White;
            rtbContent.DeselectAll();
            txtSearch.Clear();
            txtSearch.Focus();
        }

        //public static Microsoft.Office.Tools.Word.Controls.RichTextBox AddRichTextBox = rtbContent;
        private void btnWord_Click(object sender, EventArgs e)
        {
            if(dataArray.Length > 0)
            {
                //Features.wordFont = rtbContent.SelectionFont;
                //FontFactory.RegisterDirectories();
                //System.Drawing.Font currentFont = rtbContent.SelectionFont;
                //Features.wordFont = FontFactory.GetFont(currentFont.Name, currentFont.Size, BaseColor.BLACK);

                //Features.wordFont = rtbContent.SelectionFont;
                dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                Features.exportWordFile(dataArray);
            }
                
        }

        private void btnText_Click(object sender, EventArgs e)
        {
            if (dataArray.Length > 0)
            {
                dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                Features.exportTextFile(dataArray);
            }
                
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            tempString = rtbContent.Text;

            if(!string.IsNullOrEmpty(txtSearch.Text))
                rtbContent.Text = rtbContent.Text.Replace(txtSearch.Text.ToString(), txtReplace2.Text.ToString());
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            rtbContent.Text = tempString;
        }
        private void refreshTempReplace()
        {
            tempString = "";
        }

        private void btnOpenM_Click(object sender, EventArgs e)
        {
            openFileDialog2 = new OpenFileDialog();
            openFileDialog2.Filter = "Pdf files (*.pdf)|*.pdf";
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                mergePath = openFileDialog2.FileName.ToString();
                txtMergePath.Text = mergePath;
            }
            openFileDialog2.Dispose();
            Refresh();
        }

        private void btnMerge_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMergePath.Text.ToString()) != true)
            {
                int totalPage = cmbPage.Items.Count + itextsharp.getNumberOfPages(mergePath);

                for (int indexB = cmbPage.Items.Count + 1; indexB <= totalPage; indexB++)
                {
                    cmbPage.Items.Add(indexB);
                }
                //assign all data array
                dataArray = itextsharp.pdfCombineArray(dataArray, mergePath, totalPage);

                MessageBox.Show("File from: "+path+"\nmerge with: "+mergePath+"\nTotal Page= "+totalPage.ToString(), 
                    "Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("No file was choose.", "Path missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }     
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtPath.Text) != true){
                dataArray[selectedPage - 1] = rtbContent.Text.ToString();
                Features.exportExcelFile(dataArray);
            }
        }

        private void ExcelEditor_Click(object sender, EventArgs e)
        {
            this.Hide();
            Excel_Editor.ExcelEditor excelEditor = new Excel_Editor.ExcelEditor();
            excelEditor.Closed += (s, args) => this.Close();
            excelEditor.Show();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            int end = rtbContent.Text.LastIndexOf(txtSearch.Text);

            if (pointerIndex < end)
            {
                rtbContent.Find(txtSearch.Text, previousPointerIndex, rtbContent.TextLength, RichTextBoxFinds.MatchCase);
                rtbContent.SelectionBackColor = Color.Yellow;  
                previousPointerIndex = pointerIndex;

                rtbContent.Find(txtSearch.Text, pointerIndex, rtbContent.TextLength, RichTextBoxFinds.MatchCase);
                rtbContent.SelectionBackColor = Color.Aqua;
                pointerIndex = rtbContent.Text.IndexOf(txtSearch.Text, pointerIndex) + 1;  
            }
        }

        private void btnReplaceOnce_Click(object sender, EventArgs e)
        {
            int end = rtbContent.Text.LastIndexOf(txtSearch.Text);
            tempString = rtbContent.Text;

            if (pointerIndex <= end)
            {
                int found = rtbContent.Find(txtSearch.Text, previousPointerIndex, rtbContent.TextLength, RichTextBoxFinds.MatchCase);
                rtbContent.SelectionBackColor = Color.White;
                rtbContent.SelectedText = txtReplace2.Text;
                previousPointerIndex = pointerIndex;

                rtbContent.Find(txtSearch.Text, pointerIndex, rtbContent.TextLength, RichTextBoxFinds.MatchCase);
                rtbContent.SelectionBackColor = Color.Aqua;
                if(pointerIndex < end)
                    pointerIndex = rtbContent.Text.IndexOf(txtSearch.Text, pointerIndex);
            }         
        }

        private void txtSearchAndConvert_TextChanged(object sender, EventArgs e)
        {
            previousPointerIndex = 0;
            pointerIndex = 0;
        }

        //List<fontStyle> fs = new List<fontStyle>();

        //private void btnFontTest_Click(object sender, EventArgs e)
        //{
        //    //    FontDialog fontDialog2 = new FontDialog(); ;

        //    //    if (fontDialog2.ShowDialog() == DialogResult.OK & !String.IsNullOrEmpty(rtbContent.Text))
        //    //    {
        //    //        System.Drawing.Font font = fontDialog1.Font;
        //    //        rtbContent.SelectionFont = font;
        //    //        fs.Add(new fontStyle {fontSize= fontDialog1.Font.Size, fontName = fontDialog1.Font.Name, page = selectedPage, startIndex = rtbContent.SelectionIndent, valueLength = rtbContent.SelectionLength });
        //    //        //Features.fontUnderlind = fontDialog1.Font.Underline;
        //    //    }
        //}
}
}
