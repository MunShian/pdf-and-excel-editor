﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowPDFViewer
{
    class fontStyle
    {
        public float fontSize { get; set; }
        public string fontName { get; set; }
        public int page { get; set; }
        public int startIndex { get; set; }
        public int valueLength { get; set; }

    }
}
