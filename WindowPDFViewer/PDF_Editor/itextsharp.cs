﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using Font = iTextSharp.text.Font;

namespace WindowPDFViewer
{
    public static class itextsharp
    {
        public static Font pdfFont { get; set; }

        public static void removeFooter(string path)
        { 
            //Document doc = new Document();
            //doc.Open();
            ////Path to where you want the file to output
            //string outputFilePath = @"C:\Users\manhi\Desktop\EXsample2.pdf";
            ////Path to where the pdf you want to modify is
            //string inputFilePath = path;
            //try
            //{
            //    using (Stream inputPdfStream = new FileStream(inputFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            //    using (Stream outputPdfStream = new FileStream(outputFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            //    using (Stream outputPdfStream2 = new FileStream(outputFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            //    {
            //        //Opens the unmodified PDF for reading
            //        var reader = new PdfReader(inputPdfStream);
            //        //Creates a stamper to put an image on the original pdf
            //        var stamper = new PdfStamper(reader, outputPdfStream) { FormFlattening = true, FreeTextFlattening = true };

            //        //Creates an image that is the size i need to hide the text i'm interested in removing
            //        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(new Bitmap(446, 500), BaseColor.WHITE);
            //        //Sets the position that the image needs to be placed (ie the location of the text to be removed)
            //        image.SetAbsolutePosition(doc.PageSize.GetRight(180), doc.PageSize.GetBottom(30));
            //        //Adds the image to the output pdf
            //        stamper.GetOverContent(1).AddImage(image, true);
            //        //Creates the first copy of the outputted pdf
            //        stamper.Close();

            //        //Opens our outputted file for reading
            //        var reader2 = new PdfReader(outputPdfStream2);

            //        //Encrypts the outputted PDF to make it not allow Copy or Pasting
            //        //PdfEncryptor.Encrypt(
            //        //    reader2,
            //        //    outputPdfStream2,
            //        //    null,
            //        //    Encoding.UTF8.GetBytes("test"),
            //        //    PdfWriter.ALLOW_PRINTING,
            //        //    true
            //        //);
            //        //Creates the outputted final file
            //        reader2.Close();
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //doc.Close();
        }

        public static string[] pdfArray(string path)
        {
           // removeFooter(path);
            PdfReader reader = new PdfReader(path);
            string text = string.Empty;
            string[] ArrayA= new string[reader.NumberOfPages];

            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                ArrayA[page-1] = PdfTextExtractor.GetTextFromPage(reader, page);
            }
            reader.Close();
            return ArrayA;
        }

        public static string[] pdfCombineArray(string[] oldArray, string mergePath, int totalPage)
        {
            PdfReader reader = new PdfReader(mergePath);

            string[] Array = new string[totalPage];
            int newPage = 1;

            for (int indexB = 0 ; indexB < oldArray.Length ; indexB++)
            {
                Array[indexB] = oldArray[indexB];
            }

            for (int page = oldArray.Length; page < totalPage; page++)
            {
                Array[page] = PdfTextExtractor.GetTextFromPage(reader, newPage);
                newPage++;
            }

            reader.Close();
            return Array;
        }

        public static int getNumberOfPages(string path)
        {
            int PageNumber;
            PdfReader reader = new PdfReader(path);
            PageNumber = reader.NumberOfPages;
            reader.Close();
            return PageNumber;
        }

        public static string pdfTextByPageNumber(string path,int PageNumber)
        {
            PdfReader reader = new PdfReader(path);
            string text = string.Empty;
            text = PdfTextExtractor.GetTextFromPage(reader, PageNumber);
            reader.Close();
            return text;
        }
        
        public static void savePdfFile(string savePath, int page, string[] NewContent,bool footerChecked)
        {
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream( savePath, FileMode.Create));

            if(footerChecked==true)
                writer.PageEvent = new ITextEvents();
            doc.Open();

            for (int indexA=0; indexA < page; indexA++)
            {
                doc.NewPage();
                Paragraph paragraph = new Paragraph(NewContent[indexA], pdfFont);
                doc.Add(paragraph);
            }
            
            doc.Close();
        }
    }
}
