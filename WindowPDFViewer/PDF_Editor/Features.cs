﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace WindowPDFViewer
{
    public static class Features
    {
       // public static Microsoft.Office.Interop.Word.Font wordFont { get; set; }

        public static string[] removeEmptyPage(string[] ArrayA)
        {
            for (int indexA = 0; indexA < ArrayA.Length; indexA++)
            {
                if (string.IsNullOrEmpty(ArrayA[indexA]) == true)
                {
                    for (int indexB = indexA; indexB < ArrayA.Length; indexB++)
                    {
                        if (indexB != ArrayA.Length - 1)
                        {
                            ArrayA[indexB] = ArrayA[indexB + 1];
                        }
                    }
                    ArrayA = ArrayA.Take(ArrayA.Count() - 1).ToArray();
                }
            }
            return ArrayA;
        }

        public static string[] addPage(string[] ArrayB)
        {
            List<string> ListArray = ArrayB.OfType<string>().ToList();

            ListArray.Add("");

            return ListArray.ToArray();
        }

        public static void exportWordFile(string[] ArrayC)
        {
            Word.Application app = new Word.Application();
            app.Visible = false;
            Word.Document doc = app.Documents.Add();
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            Font font = new Font("Microsoft Sans Serif", 8.25f);

            saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Word File (.docx ,.doc)|*.docx;*.doc";
            saveFileDialog1.DefaultExt = "docx .doc";
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.ShowDialog();

            //save path allocate
            if (saveFileDialog1.FileName != "")
            {
                try
                {
                    for (int indexC = 0; indexC < ArrayC.Length; indexC++)
                    {
                        app.Selection.TypeText(ArrayC[indexC]);
                        //insert break but not last page
                        if (indexC < ArrayC.Length - 1)
                            app.Selection.InsertBreak(7);
                    }
                    doc.SaveAs2(saveFileDialog1.FileName.ToString());
                    MessageBox.Show("File save in: " + saveFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            app.Quit();
        }

        public static void exportTextFile(string[] ArrayD)
        {
            SaveFileDialog saveFileDialog2;
            saveFileDialog2 = new SaveFileDialog();
            saveFileDialog2.Filter = "Text File (.txt)|*.txt";
            saveFileDialog2.DefaultExt = "txt";
            saveFileDialog2.AddExtension = true;
            saveFileDialog2.ShowDialog();
            string combineString = null;

            for (int indexD = 0; indexD < ArrayD.Length; indexD++)
            {
                combineString += (ArrayD[indexD] + "\n");
            }

            //save path allocate
            if (saveFileDialog2.FileName != "")
            {
                File.WriteAllText(saveFileDialog2.FileName.ToString(), combineString);
                MessageBox.Show("File save in: " + saveFileDialog2.FileName);
            }
        }

        public static void exportExcelFile(string[] ArrayE)
        {
            SaveFileDialog saveFileDialog3 = new SaveFileDialog();
            string savePath;
            saveFileDialog3.DefaultExt = ".xlsx";
            saveFileDialog3.Filter = "(.xlsx)|*.xlsx";
            saveFileDialog3.AddExtension = true;
            saveFileDialog3.ShowDialog();

            if (saveFileDialog3.FileName != "")
            {
                savePath = saveFileDialog3.FileName;

                Excel.Application App = new Excel.Application();
                Excel.Workbook Workbook = App.Workbooks.Add();
                Excel._Worksheet Worksheet = Workbook.Sheets[1];

                for (int tempIndex = 0; tempIndex < ArrayE.Length; tempIndex++)
                {

                    if (tempIndex != 0)
                    {
                        Worksheet = Workbook.Sheets.Add(After: Workbook.Sheets[Workbook.Sheets.Count]);
                    }
                    var line = ArrayE[tempIndex].Split(new string[] { "\n" }, StringSplitOptions.None);

                    for (int IndexRow = 0; IndexRow < line.Length; IndexRow++)
                    {

                        try
                        {
                            Worksheet.Cells[1][IndexRow + 1] = line[IndexRow];
                        }
                        catch (Exception ex) { };

                    }
                }


                if (App.ActiveWorkbook != null)
                {
                    try
                    {
                        App.ActiveWorkbook.SaveAs(savePath);
                        MessageBox.Show("Saved.\nThe data was store in: " + savePath.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }

                if (App != null)
                {
                    if (Workbook != null)
                    {
                        if (Workbook != null)

                            Marshal.ReleaseComObject(Workbook);

                        Workbook.Close(false, Type.Missing, Type.Missing);

                        Marshal.ReleaseComObject(Workbook);
                    }
                    App.Quit();

                    Marshal.ReleaseComObject(App);
                }

            }
        }
        //public static string ReplaceFirst(this string text, string search, string replace)
        //{
        //    int pos = text.IndexOf(search);
        //    if (pos < 0)
        //    {
        //        return text;
        //    }
        //    return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        //}
    }
}
