﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowPDFViewer.Excel_Editor
{
    public static class Office_Excel
    {
        static List<Excel_Chart> ExcelChart = ExcelEditor.EC;
        static List<Excel_Chart> ExcelChart2 = ExcelEditor.EC2;

        public static string getMaxColumnAndRow(string excelPath)
        {
            int maxColumn = 0;
            int maxRow = 0;
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Open(excelPath, 0, true, 5,
                "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            for (int index = 1; index <= excelBook.Sheets.Count; index++)
            {
                Excel.Worksheet excelSheet = (Excel.Worksheet)excelBook.Worksheets.get_Item(index);
                Excel.Range excelRange = excelSheet.UsedRange;

                if (excelRange.Columns.Count > maxColumn)
                    maxColumn = excelRange.Columns.Count;
                if (excelRange.Rows.Count > maxRow)
                    maxRow = excelRange.Rows.Count;
            }

            if (excelApp != null)
            {
                if (excelBook != null)
                {
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);   
                }
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
            }

            return maxColumn + "|" + maxRow;
        }

        public static string[,,] excelArray(string excelPath, int maxRow, int maxColumn)
        {
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Open(excelPath, 0, true, 5,
                "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            // sheet count no + 1 *
            string[,,] ArrayA = new string[excelBook.Sheets.Count, maxRow, maxColumn];

            for (int page = 1; page <= excelBook.Sheets.Count; page++)
            {
                Excel.Worksheet excelSheet = (Excel.Worksheet)excelBook.Worksheets.get_Item(page);
                Excel.Range excelRange = excelSheet.UsedRange;

                for (int rowCnt = 1; rowCnt <= excelRange.Rows.Count; rowCnt++)
                {
                    string strData = "";
                    double douData;
                    for (int colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                    {
                        try
                        {
                            strData = (string)(excelRange.Cells[rowCnt, colCnt] as Excel.Range).Value2;
                            ArrayA[page - 1, rowCnt - 1, colCnt - 1] = strData;
                        }
                        catch (Exception ex)
                        {
                            douData = (excelRange.Cells[rowCnt, colCnt] as Excel.Range).Value2;
                            ArrayA[page - 1, rowCnt - 1, colCnt - 1] = douData.ToString();
                        }
                    }
                }
            }
            if (excelApp != null)
            {
                if (excelBook != null)
                {
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);
                }
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
            }

            return ArrayA;
        }

        public static void saveExcel(string[,,] excelArray, string savePath, int dgvNumber)
        {
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Add();
            Excel._Worksheet excelSheet = excelBook.Sheets[1];
            excelApp.DisplayAlerts = false;

            for (int tempIndex = 0; tempIndex < excelArray.GetLength(0); tempIndex++)
            {
                if (tempIndex != 0)
                {
                    excelSheet = excelBook.Sheets.Add(After: excelBook.Sheets[excelBook.Sheets.Count]);
                }

                for (int IndexRow = 0; IndexRow < excelArray.GetLength(1); IndexRow++)
                {

                    for (int IndexColumn = 0; IndexColumn < excelArray.GetLength(2); IndexColumn++)
                    {
                        try
                        {
                            excelSheet.Cells[IndexColumn + 1][IndexRow + 1] = excelArray[tempIndex, IndexRow, IndexColumn];
                        }
                        catch (Exception ex) { };

                    }
                }
            }

            if (ExcelChart.Count > 0 && dgvNumber == 1)
            {
                for(int index=0;index< ExcelChart.Count; index++)
                {
                    if (ExcelChart[index].chartType != "None")
                    {
                        Excel.Worksheet excelSheet2 = (Excel.Worksheet)excelBook.Worksheets.get_Item(ExcelChart[index].page);
                        Excel.Range chartRange;

                        Excel.ChartObjects excelCharts = (Excel.ChartObjects)excelSheet2.ChartObjects(Type.Missing);
                        Excel.ChartObject myChart = (Excel.ChartObject)excelCharts.Add(10, 80, 300, 250);
                        Excel.Chart chartPage = myChart.Chart;
                        Excel.Range c1 = excelSheet2.Cells[ExcelChart[index].startRow + 1, ExcelChart[index].startColumn + 1];
                        Excel.Range c2 = excelSheet2.Cells[ExcelChart[index].endRow + 1, ExcelChart[index].endColumn + 1];

                        chartRange = (Excel.Range)excelSheet2.get_Range(c1, c2);
                        chartPage.SetSourceData(chartRange, System.Reflection.Missing.Value);

                        switch (ExcelChart[index].chartType)
                        {
                            case "ColumnClustered":
                                chartPage.ChartType = Excel.XlChartType.xlColumnClustered;
                                break;

                            case "ColumnStacked":
                                chartPage.ChartType = Excel.XlChartType.xlColumnStacked;
                                break;

                            case "Pie":
                                chartPage.ChartType = Excel.XlChartType.xlPie;
                                break;

                            case "Line":
                                chartPage.ChartType = Excel.XlChartType.xlLine;
                                break;

                            case "CylinderBarStacked":
                                chartPage.ChartType = Excel.XlChartType.xlCylinderBarStacked;
                                break;
                        }
                    }  
                          
                }
               
            }else if (ExcelChart2.Count > 0 && dgvNumber == 2)
            {
                for (int index = 0; index < ExcelChart2.Count; index++)
                {
                    //1th: check the chart is reject by user,   2th: check belong to which datagridview,   3h: check this event trigger by which dgv
                    if (ExcelChart2[index].chartType != "None")
                    {
                        Excel.Worksheet excelSheet2 = (Excel.Worksheet)excelBook.Worksheets.get_Item(ExcelChart2[index].page);
                        Excel.Range chartRange;

                        Excel.ChartObjects excelCharts = (Excel.ChartObjects)excelSheet2.ChartObjects(Type.Missing);
                        Excel.ChartObject myChart = (Excel.ChartObject)excelCharts.Add(10, 80, 300, 250);
                        Excel.Chart chartPage = myChart.Chart;
                        Excel.Range c1 = excelSheet2.Cells[ExcelChart2[index].startRow + 1, ExcelChart2[index].startColumn + 1];
                        Excel.Range c2 = excelSheet2.Cells[ExcelChart2[index].endRow + 1, ExcelChart2[index].endColumn + 1];

                        chartRange = (Excel.Range)excelSheet2.get_Range(c1, c2);
                        chartPage.SetSourceData(chartRange, System.Reflection.Missing.Value);

                        switch (ExcelChart2[index].chartType)
                        {
                            case "ColumnClustered":
                                chartPage.ChartType = Excel.XlChartType.xlColumnClustered;
                                break;

                            case "ColumnStacked":
                                chartPage.ChartType = Excel.XlChartType.xlColumnStacked;
                                break;

                            case "Pie":
                                chartPage.ChartType = Excel.XlChartType.xlPie;
                                break;

                            case "Line":
                                chartPage.ChartType = Excel.XlChartType.xlLine;
                                break;

                            case "CylinderBarStacked":
                                chartPage.ChartType = Excel.XlChartType.xlCylinderBarStacked;
                                break;
                        }
                    }
                }
            }

            if (excelApp.ActiveWorkbook != null)
            {
                excelApp.ActiveWorkbook.SaveAs(savePath);
            }

            if (excelApp != null)
            {
                if (excelBook != null)
                {
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);
                }
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
            }
        }

    }
}
