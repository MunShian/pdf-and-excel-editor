﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace WindowPDFViewer.Excel_Editor
{
    public static class Features_Algorithm
    {
        public static DataTable getExcelTable(string[,,] excelArray, int pageIndex)
        {
            DataTable dt = new DataTable();

            for (int colCnt = 0; colCnt < excelArray.GetLength(2); colCnt++)
            {
                string strColumn = "";
                dt.Columns.Add(strColumn, typeof(string));
            }

            for (int rowCnt = 0; rowCnt < excelArray.GetLength(1); rowCnt++)
            {
                string strData = "";
                for (int colCnt = 0; colCnt < excelArray.GetLength(2); colCnt++)
                {
                    strData += excelArray[pageIndex, rowCnt, colCnt] + "|";
                }
                strData = strData.Remove(strData.Length - 1, 1);
                dt.Rows.Add(strData.Split('|'));
            }
            return dt;
        }

        public static string[,,] upExcelArray(DataTable dt,string[,,]excelArray, int currentPageNumber)
        {
            for (int RowIndex = 0; RowIndex < dt.Rows.Count; RowIndex++)
            {
                for (int ColumnIndex = 0; ColumnIndex < dt.Columns.Count; ColumnIndex++)
                {
                        excelArray[currentPageNumber-1, RowIndex, ColumnIndex] = dt.Rows[RowIndex][ColumnIndex].ToString();
                }
            }
            return excelArray;
        }
        public static int[] getTotalSheetArray(int totalPage)
        {
            int[] page = new int[totalPage];
            for (int index = 1; index <= totalPage; index++)
            {
                page[index - 1] = index;

            }

            return page;
        }
        public static string[,] setCopyData(int startRow, int startColumn, int endRow, int endColumn, int currentPageNumber, string[,,] excelArray)
        {

            string[,] copyArray = new string[(endRow - startRow + 1),(endColumn - startColumn + 1)];
            int IndexRow = 0;
            int IndexColumn = 0;

            for (int Row = startRow; Row <= endRow; Row++)
            {
                for (int Column = startColumn; Column <= endColumn; Column++)
                {
                    copyArray[IndexRow, IndexColumn] = excelArray[currentPageNumber, Row, Column];
                    IndexColumn++;
                }
                IndexRow++;
                IndexColumn = 0;
            }

            return copyArray;
        }

        public static string[,,] pasteArray(int currentPageNumber, string[,] copyArray, string[,,] excelArray, int PasteRow, int PasteColumn)
        {
            int StartupPasteColumn = PasteColumn;

            for (int rowCnt = 0; rowCnt < copyArray.GetLength(0); rowCnt++)
            {
                if (PasteRow < excelArray.GetLength(1))
                {
                    for (int colCnt = 0; colCnt < copyArray.GetLength(1); colCnt++)
                    {
                        if (PasteColumn < excelArray.GetLength(2))
                        {
                            excelArray[currentPageNumber, PasteRow, PasteColumn] = copyArray[rowCnt, colCnt];
                            PasteColumn++;
                        }
                    }
                    PasteColumn = StartupPasteColumn;
                    PasteRow++;
                }
            }
            return excelArray;
        }

        public static DataTable sumTable(string[,] copyData)
        {
            double[] sumTotalArray = new double[copyData.GetLength(1)];

            for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
            {
                double sumTotal = 0.0;

                for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (isNumeric == true)
                    {
                        sumTotal += double.Parse(copyData[rowCnt, colCnt]);
                    }
                }
                sumTotalArray[colCnt] = sumTotal;
            }

            DataTable dt = new DataTable();
            dt = arrayToTable(sumTotalArray);

            return dt;
        }

        public static DataTable averageTable(string[,] copyData)
        {
            double[] averageTotalArray = new double[copyData.GetLength(1)];

            for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
            {
                double sumTotal = 0.0;
                int numericCnt = 0;

                for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (isNumeric == true && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        numericCnt++;
                        sumTotal += double.Parse(copyData[rowCnt, colCnt]);
                    }
                }
                averageTotalArray[colCnt] = Math.Round((sumTotal / numericCnt), 3);
            }

            DataTable dt = new DataTable();
            dt = arrayToTable(averageTotalArray);

            return dt;
        }

        public static DataTable countTable(string[,] copyData)
        {
            double[] countTotalArray = new double[copyData.GetLength(1)];
            for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
            {
                int numericCnt = 0;

                for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (isNumeric == true)
                    {
                        numericCnt++;
                    }
                }
                countTotalArray[colCnt] = numericCnt;
            }

            DataTable dt = new DataTable();
            dt = arrayToTable(countTotalArray);

            return dt; 
        }

        public static DataTable percentTable(string[,] copyData)
        {
            double[] sumTotalArray = new double[copyData.GetLength(1)];
            double totalValue = 0.0;

            for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
            {
                double sumTotal = 0.0;

                for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (isNumeric == true)
                    {
                        sumTotal += double.Parse(copyData[rowCnt, colCnt]);
                    }
                }
                sumTotalArray[colCnt] = sumTotal;
                totalValue += sumTotal;
            }

            double[] percentTotalArray = new double[sumTotalArray.Length];

            for(int colCnt = 0; colCnt < sumTotalArray.Length; colCnt++)
            {
                percentTotalArray[colCnt] = Math.Round((sumTotalArray[colCnt] * 100) / totalValue, 3);
            } 

            DataTable dt = new DataTable();

            for (int colCnt = 0; colCnt < percentTotalArray.Length; colCnt++)
            {
                string strColumn = "";
                dt.Columns.Add(strColumn, typeof(string));
            }

            string strData = "";
            for (int colCnt = 0; colCnt < percentTotalArray.Length; colCnt++)
            {
                strData += percentTotalArray[colCnt] + "%|";
            }
            strData = strData.Remove(strData.Length - 1, 1);
            dt.Rows.Add(strData.Split('|'));

            return dt;
        }

        public static DataTable runTotalTable(string[,] copyData)
        {
            double[] sumTotalArray = new double[copyData.GetLength(1)];

            for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
            {
                double sumTotal = 0.0;

                for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (isNumeric == true)
                    {
                        sumTotal += double.Parse(copyData[rowCnt, colCnt]);
                    }
                }
                if (colCnt != 0)
                    sumTotalArray[colCnt] = sumTotal + sumTotalArray[colCnt - 1];
                else
                    sumTotalArray[colCnt] = sumTotal;
            }

            DataTable dt = new DataTable();
            dt = arrayToTable(sumTotalArray);

            return dt;
        }

        public static DataTable filterTable(string[,] copyData, bool checkCharacter)
        {
            string[,] filterArray = new string[copyData.GetLength(0),copyData.GetLength(1)]; 

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    var isNumeric = double.TryParse(copyData[rowCnt, colCnt], out double n);

                    if (checkCharacter == true)
                    {
                        if (isNumeric == false)
                        {
                            filterArray[rowCnt, colCnt] = copyData[rowCnt, colCnt].ToString();
                        }
                    }
                    else
                    {
                        if (isNumeric == true)
                        {
                            filterArray[rowCnt, colCnt] = copyData[rowCnt, colCnt].ToString();
                        }
                    }
                    
                }

            }
            DataTable dt = new DataTable();

            for (int colCnt = 0; colCnt < filterArray.GetLength(1); colCnt++)
            {
                string strColumn = "";
                dt.Columns.Add(strColumn, typeof(string));
            }

            for (int rowCnt = 0; rowCnt < filterArray.GetLength(0); rowCnt++)
            {
                string strData = "";
                for (int colCnt = 0; colCnt < filterArray.GetLength(1); colCnt++)
                {
                    strData += filterArray[rowCnt, colCnt] + "|";
                }
                strData = strData.Remove(strData.Length - 1, 1);
                dt.Rows.Add(strData.Split('|'));
            }

            return dt;
        }

     
        private static DataTable arrayToTable(double[] douArray)
        {
            DataTable dt = new DataTable();

            for (int colCnt = 0; colCnt < douArray.Length; colCnt++)
            {
                string strColumn = "";
                dt.Columns.Add(strColumn, typeof(string));
            }

            string strData = "";
            for (int colCnt = 0; colCnt < douArray.Length; colCnt++)
            {
                strData += douArray[colCnt] + "|";
            }
            strData = strData.Remove(strData.Length - 1, 1);
            dt.Rows.Add(strData.Split('|'));

            return dt;
        }

        public static DataTable decToHexTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    var isDecimal = decimal.TryParse(copyData[rowCnt, colCnt], out decimal n);

                    if (isDecimal == true && !copyData[rowCnt, colCnt].Contains('.'))
                    {
                        try
                        {
                            copyData[rowCnt, colCnt] = decimalToHexaDecimal(long.Parse(copyData[rowCnt, colCnt]));
                        }
                        catch(Exception ex){}
                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }
        
        public static DataTable hexToDecTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    //var isNumeric = int.TryParse(copyData[rowCnt, colCnt], out int n);

                    if (!copyData[rowCnt, colCnt].Contains('.') && IsHex(copyData[rowCnt, colCnt]) 
                        && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        try
                        {
                            copyData[rowCnt, colCnt] = Convert.ToInt64(copyData[rowCnt, colCnt], 16).ToString();
                        }
                        catch (Exception){}
                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }

        public static DataTable decToBinTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    var isDecimal = decimal.TryParse(copyData[rowCnt, colCnt], out decimal n);

                    if (isDecimal == true && !copyData[rowCnt, colCnt].Contains('.')  && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        try
                        { 
                            copyData[rowCnt, colCnt] = Convert.ToString(long.Parse(copyData[rowCnt, colCnt]), 2);
                        }
                        catch (Exception) { }
                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }
       
        public static DataTable binToDecTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt<copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt<copyData.GetLength(1); colCnt++)
                {
                    var isNumeric = int.TryParse(copyData[rowCnt, colCnt], out int n);

                    if (isNumeric == true && !copyData[rowCnt, colCnt].Contains('.')
                        && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        try
                        {
                            copyData[rowCnt, colCnt] = (Convert.ToInt64(copyData[rowCnt, colCnt], 2)).ToString();
                        }
                        catch (Exception){}

                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }

        public static DataTable decToOctTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    var isNumeric = int.TryParse(copyData[rowCnt, colCnt], out int n);

                    if (isNumeric == true && !copyData[rowCnt, colCnt].Contains('.')
                        && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        try
                        {
                            copyData[rowCnt, colCnt] = Convert.ToString(long.Parse(copyData[rowCnt, colCnt]), 8);
                        }
                        catch (Exception) { }
                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }
        public static DataTable octToDecTable(string[,] copyData)
        {
            DataTable dt = new DataTable();

            for (int rowCnt = 0; rowCnt < copyData.GetLength(0); rowCnt++)
            {
                for (int colCnt = 0; colCnt < copyData.GetLength(1); colCnt++)
                {
                    var isNumeric = int.TryParse(copyData[rowCnt, colCnt], out int n);

                    if (isNumeric == true && !copyData[rowCnt, colCnt].Contains('.')
                        && !string.IsNullOrEmpty(copyData[rowCnt, colCnt]))
                    {
                        try
                        {
                            copyData[rowCnt, colCnt] = (Convert.ToInt64(copyData[rowCnt, colCnt], 8)).ToString();
                        }
                        catch (Exception) { }
                    }
                }
            }
            dt = stringArrayToTable(copyData);
            return dt;
        }

        private static DataTable stringArrayToTable(string[,] stringArray)
        {
            DataTable dt = new DataTable();

            for (int colCnt = 0; colCnt < stringArray.GetLength(1); colCnt++)
            {
                string strColumn = "";
                dt.Columns.Add(strColumn, typeof(string));
            }

            for (int rowCnt = 0; rowCnt < stringArray.GetLength(0); rowCnt++)
            {
                string strData = "";
                for (int colCnt = 0; colCnt < stringArray.GetLength(1); colCnt++)
                {
                    strData += stringArray[rowCnt, colCnt] + "|";
                }
                strData = strData.Remove(strData.Length - 1, 1);
                dt.Rows.Add(strData.Split('|'));
            }

            return dt;
        }

        private static bool IsHex(IEnumerable<char> chars)
        {
            bool isHex;
            foreach (var c in chars)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'a' && c <= 'f') ||
                         (c >= 'A' && c <= 'F'));

                if (!isHex)
                    return false;
            }
            return true;
        }     

        private static string decimalToHexaDecimal(long quotient)
        {
            string hexValue = "";

            long i = 1, j, temp = 0;
            char[] hexadecimalNumber = new char[100];
            char temp1;

            while (quotient != 0)
            {
                temp = quotient % 16;
                if (temp < 10)
                    temp = temp + 48;
                else
                    temp = temp + 55;
                temp1 = Convert.ToChar(temp);
                hexadecimalNumber[i++] = temp1;
                quotient = quotient / 16;
            }
            for (j = i - 1; j > 0; j--)
                hexValue += hexadecimalNumber[j].ToString();

            return hexValue;
        }

        public static string[] fillCmbChart()
        {
            string[] cmbChartArray = new string[5];
            cmbChartArray[0] = "ColumnClustered";
            cmbChartArray[1] = "ColumnStacked";
            cmbChartArray[2] = "Pie";
            cmbChartArray[3] = "Line";
            cmbChartArray[4] = "CylinderBarStacked";

            return cmbChartArray;
        }
    }
}
