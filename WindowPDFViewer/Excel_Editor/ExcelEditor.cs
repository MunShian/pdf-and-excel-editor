﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowPDFViewer.Excel_Editor
{
    public partial class ExcelEditor : Form
    {
        private string excelPath;
        private string saveFilePath;
        private string[,,] excelArray;
        private int currentPageNumber = 1;

        private string[,] copyData;

        private string excelPath2;
        private string saveFilePath2;
        private string[,,] excelArray2;
        private int currentPageNumber2 = 1;
        public static List<Excel_Chart> EC = new List<Excel_Chart>();
        public static List<Excel_Chart> EC2 = new List<Excel_Chart>();

        private int count = 0;
        private int count2 = 0;
        private int btnIndex = 0;

        public ExcelEditor()
        {
            InitializeComponent();
            clearChartTable(1);
            clearChartTable(2);
        }

        private void clearChartTable(int dgvNum)
        {
            if (dgvNum == 1)
            {
                while (tblChart.Controls.Count > 0)
                {
                    tblChart.Controls[0].Dispose();
                }
                tblChart.Controls.Clear();
            }
            else
            {
                while (tblChart2.Controls.Count > 0)
                {
                    tblChart2.Controls[0].Dispose();
                }
                tblChart2.Controls.Clear();
            }
            
        }


        private void btnOpen_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.DefaultExt = ".xlsx";
            openFileDialog1.Filter = "(.xlsx)|*.xlsx";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                EC.Clear();
                clearChartTable(1);
                currentPageNumber = 1;
                excelPath = openFileDialog1.FileName.ToString();
                txtPath.Text = excelPath;
                string[] number = Office_Excel.getMaxColumnAndRow(excelPath).Split('|');
                int maxColumn = int.Parse(number[0]);
                int maxRow = int.Parse(number[1]);
                excelArray = Office_Excel.excelArray(excelPath,maxRow,maxColumn);               
                dt = Features_Algorithm.getExcelTable(excelArray, 0);
                dgvExcel.DataSource = dt.DefaultView;
                cmbPage.DataSource = Features_Algorithm.getTotalSheetArray(excelArray.GetLength(0));
                cmbChart.DataSource = Features_Algorithm.fillCmbChart();
            }
        }

        private void cmbPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateExcelArray();
            currentPageNumber = int.Parse(cmbPage.SelectedItem.ToString());
            DataTable dt = new DataTable();
            dt = Features_Algorithm.getExcelTable(excelArray, cmbPage.SelectedIndex);
            dgvExcel.DataSource = dt.DefaultView;
        }

        private void updateExcelArray()
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                DataTable dt = new DataTable();
                dt = ((DataView)dgvExcel.DataSource).Table;
                excelArray = Features_Algorithm.upExcelArray(dt, excelArray, currentPageNumber);
            }         
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                updateExcelArray();
                Office_Excel.saveExcel(excelArray, excelPath, 1);
                MessageBox.Show("Excel file save", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveAs_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.DefaultExt = ".xlsx";
                saveFileDialog1.Filter = "(.xlsx)|*.xlsx";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    updateExcelArray();
                    saveFilePath = saveFileDialog1.FileName.ToString();
                    //Office_Excel.clusterChart(excelArray, saveFilePath, 1);
                    Office_Excel.saveExcel(excelArray, saveFilePath, 1);
                    MessageBox.Show("Excel file save at " + saveFilePath, "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnOpen2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            OpenFileDialog openFileDialog2 = new OpenFileDialog();
            openFileDialog2.DefaultExt = ".xlsx";
            openFileDialog2.Filter = "(.xlsx)|*.xlsx";

            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                EC2.Clear();
                clearChartTable(2);
                currentPageNumber2 = 1;
                excelPath2 = openFileDialog2.FileName.ToString();
                txtPath2.Text = excelPath2;
                string[] number = Office_Excel.getMaxColumnAndRow(excelPath2).Split('|');
                int maxColumn = int.Parse(number[0]);
                int maxRow = int.Parse(number[1]);
                excelArray2 = Office_Excel.excelArray(excelPath2, maxRow, maxColumn);
                dt = Features_Algorithm.getExcelTable(excelArray2, 0);
                dgvExcel2.DataSource = dt.DefaultView;
                cmbPage2.DataSource = Features_Algorithm.getTotalSheetArray(excelArray2.GetLength(0));
                cmbChart2.DataSource = Features_Algorithm.fillCmbChart();
            }
        }

        private void updateExcelArray2()
        {
            if (!string.IsNullOrEmpty(txtPath2.Text))
            {
                DataTable dt = new DataTable();
                dt = ((DataView)dgvExcel2.DataSource).Table;
                excelArray2 = Features_Algorithm.upExcelArray(dt, excelArray2, currentPageNumber2);
            }
        }

        private void cmbPage2_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateExcelArray2();
            currentPageNumber2 = int.Parse(cmbPage2.SelectedItem.ToString());
            DataTable dt = new DataTable();
            dt = Features_Algorithm.getExcelTable(excelArray2, cmbPage2.SelectedIndex);
            dgvExcel2.DataSource = dt.DefaultView;
        }

        private void btnSave2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath2))
            {
                updateExcelArray2();
                Office_Excel.saveExcel(excelArray2, excelPath2, 2);
                MessageBox.Show("Excel file save", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSaveAs2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath2))
            {
                SaveFileDialog saveFileDialog2 = new SaveFileDialog();
                saveFileDialog2.DefaultExt = ".xlsx";
                saveFileDialog2.Filter = "(.xlsx)|*.xlsx";

                if (saveFileDialog2.ShowDialog() == DialogResult.OK)
                {
                    updateExcelArray2();
                    saveFilePath2 = saveFileDialog2.FileName.ToString();
                    Office_Excel.saveExcel(excelArray2, saveFilePath2, 2);
                    MessageBox.Show("Excel file save at " + saveFilePath2, "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            EC.Clear();
            EC2.Clear();
            this.Hide();
            Form1 FormPDF = new Form1();
            FormPDF.Closed += (s, args) => this.Close();
            FormPDF.Show();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                updateExcelArray();
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;

                Int32 selectedCellCount = dgvExcel.GetCellCount(DataGridViewElementStates.Selected);
                if (selectedCellCount > 0)
                {
                    int smallestRow = dgvExcel.SelectedCells[selectedCellCount-1].RowIndex;
                    int smallestColumn = dgvExcel.SelectedCells[selectedCellCount-1].ColumnIndex;
                    int biggestRow = 0;
                    int biggestColumn = 0;

                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        endRow = dgvExcel.SelectedCells[index].RowIndex;
                        endColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                        if (biggestRow < endRow)
                            biggestRow = endRow;
                        if (biggestColumn < endColumn)
                            biggestColumn = endColumn;
                    }
                    endRow = biggestRow;
                    endColumn = biggestColumn;
                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        startRow = dgvExcel.SelectedCells[index].RowIndex;
                        startColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                        if (smallestRow > startRow)
                            smallestRow = startRow;
                        if (smallestColumn > startColumn)
                            smallestColumn = startColumn;
                    }
                    startRow = smallestRow;
                    startColumn = smallestColumn;
                }

                copyData = Features_Algorithm.setCopyData(startRow, startColumn, endRow, endColumn, cmbPage.SelectedIndex, excelArray);
            }
        }

        private void btnCopy2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath2))
            {
                updateExcelArray2();
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;

                Int32 selectedCellCount = dgvExcel2.GetCellCount(DataGridViewElementStates.Selected);
                if (selectedCellCount > 0)
                {
                    int smallestRow = dgvExcel2.SelectedCells[selectedCellCount - 1].RowIndex;
                    int smallestColumn = dgvExcel2.SelectedCells[selectedCellCount - 1].ColumnIndex;
                    int biggestRow = 0;
                    int biggestColumn = 0;

                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        endRow = dgvExcel2.SelectedCells[index].RowIndex;
                        endColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                        if (biggestRow < endRow)
                            biggestRow = endRow;
                        if (biggestColumn < endColumn)
                            biggestColumn = endColumn;
                    }
                    endRow = biggestRow;
                    endColumn = biggestColumn;
                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        startRow = dgvExcel2.SelectedCells[index].RowIndex;
                        startColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                        if (smallestRow > startRow)
                            smallestRow = startRow;
                        if (smallestColumn > startColumn)
                            smallestColumn = startColumn;
                    }
                    startRow = smallestRow;
                    startColumn = smallestColumn;
                }
                copyData = Features_Algorithm.setCopyData(startRow, startColumn, endRow, endColumn, cmbPage2.SelectedIndex, excelArray2);
            }
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath.Text))
            {
                DataTable dt = new DataTable();
                updateExcelArray();
                int PasteRow = dgvExcel.SelectedCells[0].RowIndex;
                int PasteColumn = dgvExcel.SelectedCells[0].ColumnIndex;

                excelArray2 = Features_Algorithm.pasteArray(cmbPage.SelectedIndex, copyData, excelArray, PasteRow, PasteColumn);

                dt = Features_Algorithm.getExcelTable(excelArray, cmbPage.SelectedIndex);
                dgvExcel.DataSource = dt.DefaultView;
            }
        }

        private void btnPaste2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath2.Text))
            {
                DataTable dt = new DataTable();
                updateExcelArray2();
                int PasteRow = dgvExcel2.SelectedCells[0].RowIndex;
                int PasteColumn = dgvExcel2.SelectedCells[0].ColumnIndex;

                excelArray2 = Features_Algorithm.pasteArray(cmbPage2.SelectedIndex, copyData, excelArray2, PasteRow, PasteColumn);

                dt = Features_Algorithm.getExcelTable(excelArray2, cmbPage2.SelectedIndex);
                dgvExcel2.DataSource = dt.DefaultView;
            }
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.sumTable(copyData);
                dgvTotal.DataSource = dt.DefaultView;
            }
        }

        private void btnCopyTotal_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                DataTable dt = new DataTable();
                dt = ((DataView)dgvTotal.DataSource).Table;

                string[,] copyData = new string[1, dt.Columns.Count];

                for (int colCnt = 0; colCnt < dt.Columns.Count; colCnt++)
                {
                    copyData[0, colCnt] = dt.Rows[0][colCnt].ToString();
                }
                this.copyData = copyData;
            }
        }

        private void btnAverage_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.averageTable(copyData);
                dgvTotal.DataSource = dt.DefaultView;
            }
        }

        private void btnCount_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.countTable(copyData);
                dgvTotal.DataSource = dt.DefaultView;
            }
        }

        private void btnPercent_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.percentTable(copyData);
                dgvTotal.DataSource = dt.DefaultView;
            }
        }

        private void btnTotal_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.runTotalTable(copyData);
                dgvTotal.DataSource = dt.DefaultView;
            }
        }

        private void btnCharacter_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                bool checkCharacter = true;

                dt = Features_Algorithm.filterTable(copyData, checkCharacter);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnCopyFilter_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                DataTable dt = new DataTable();
                dt = ((DataView)dgvFilter.DataSource).Table;

                string[,] copyData = new string[dt.Rows.Count, dt.Columns.Count];

                for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                {
                    for (int colCnt = 0; colCnt < dt.Columns.Count; colCnt++)
                    {
                        copyData[rowCnt, colCnt] = dt.Rows[rowCnt][colCnt].ToString();
                    }
                }
                this.copyData = copyData;
            }
        }

        private void btnNumeric_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                bool checkCharacter = false;

                dt = Features_Algorithm.filterTable(copyData, checkCharacter);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnDecToOct_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "Octal";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.decToOctTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnDecToHex_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "HexaDecimal";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.decToHexTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnHexToDec_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "Decimal";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.hexToDecTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnDecToBin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "Binary";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.decToBinTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnBinToDec_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "Decimal";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.binToDecTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private void btnOctToDec_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                lblType.Text = "Decimal";
                btnCopy_Click(sender, e);
                DataTable dt = new DataTable();
                dt = Features_Algorithm.octToDecTable(copyData);
                dgvFilter.DataSource = dt.DefaultView;
            }
        }

        private ToolStripMenuItem Copy = new ToolStripMenuItem();
        private ToolStripMenuItem Paste = new ToolStripMenuItem();
        private ToolStripMenuItem Chart = new ToolStripMenuItem();
        private ToolStripMenuItem Clear = new ToolStripMenuItem();
        private ContextMenuStrip strip;

        private ToolStripMenuItem Copy2 = new ToolStripMenuItem();
        private ToolStripMenuItem Paste2 = new ToolStripMenuItem();
        private ToolStripMenuItem Chart2 = new ToolStripMenuItem();
        private ToolStripMenuItem Clear2 = new ToolStripMenuItem();
        private ContextMenuStrip strip2;
       //private string cellErrorText;

        private void dgvExcel_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs c)
        {
            //cellErrorText = String.Empty;

            if (strip == null)
            {
                strip = new ContextMenuStrip();
                Copy.Text = "Copy";
                Paste.Text = "Paste";
                Clear.Text = "Clear";
                Chart.Text = "Add Chart";
                strip.Items.Add(Copy);
                strip.Items.Add(Paste);
                strip.Items.Add(Clear);
                strip.Items.Add(Chart);
                Copy.Click += (s, e) => btnCopy_Click(sender, e);
                Paste.Click += (s, e) => btnPaste_Click(sender, e);
                Clear.Click += (s, e) => clearCells(sender, e);
                Chart.Click += (s, e) => btnChart_Click(sender, e);
            }
            c.ContextMenuStrip = strip;
        }

        private void dgvExcel2_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs c)
        {
            //cellErrorText = String.Empty;

            if (strip2 == null)
            {
                strip2 = new ContextMenuStrip();
                Copy2.Text = "Copy";
                Paste2.Text = "Paste";
                Clear2.Text = "Clear";
                Chart2.Text = "Add Chart";
                strip2.Items.Add(Copy2);
                strip2.Items.Add(Paste2);
                strip2.Items.Add(Clear2);
                strip2.Items.Add(Chart2);
                Copy2.Click += (s, e) => btnCopy2_Click(sender, e);
                Paste2.Click += (s, e) => btnPaste2_Click(sender, e);
                Clear2.Click += (s, e) => clearCells2(sender, e);
                Chart2.Click += (s, e) => btnChart2_Click(sender, e);
            }
            c.ContextMenuStrip = strip2;
        }

        private void clearCells(object sender, EventArgs e)
        {
            updateExcelArray();
            int startRow = 0;
            int startColumn = 0;
            int endRow = 0;
            int endColumn = 0;

            Int32 selectedCellCount = dgvExcel.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 0)
            {
                int smallestRow = dgvExcel.SelectedCells[selectedCellCount - 1].RowIndex;
                int smallestColumn = dgvExcel.SelectedCells[selectedCellCount - 1].ColumnIndex;
                int biggestRow = 0;
                int biggestColumn = 0;

                for (int index = 0; index < selectedCellCount; index++)
                {
                    endRow = dgvExcel.SelectedCells[index].RowIndex;
                    endColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                    if (biggestRow < endRow)
                        biggestRow = endRow;
                    if (biggestColumn < endColumn)
                        biggestColumn = endColumn;
                }
                endRow = biggestRow;
                endColumn = biggestColumn;
                for (int index = 0; index < selectedCellCount; index++)
                {
                    startRow = dgvExcel.SelectedCells[index].RowIndex;
                    startColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                    if (smallestRow > startRow)
                        smallestRow = startRow;
                    if (smallestColumn > startColumn)
                        smallestColumn = startColumn;
                }
                startRow = smallestRow;
                startColumn = smallestColumn;
            }

            for (int Row = startRow; Row <= endRow; Row++)
            {
                for (int Column = startColumn; Column <= endColumn; Column++)
                {
                    excelArray[currentPageNumber-1, Row, Column]="";
                }
            }
            DataTable dt = new DataTable();
            dt = Features_Algorithm.getExcelTable(excelArray, cmbPage.SelectedIndex);
            dgvExcel.DataSource = dt.DefaultView;
        }

        private void clearCells2(object sender, EventArgs e)
        {
            updateExcelArray2();
            int startRow = 0;
            int startColumn = 0;
            int endRow = 0;
            int endColumn = 0;

            Int32 selectedCellCount = dgvExcel2.GetCellCount(DataGridViewElementStates.Selected);
            if (selectedCellCount > 0)
            {
                int smallestRow = dgvExcel2.SelectedCells[selectedCellCount - 1].RowIndex;
                int smallestColumn = dgvExcel2.SelectedCells[selectedCellCount - 1].ColumnIndex;
                int biggestRow = 0;
                int biggestColumn = 0;

                for (int index = 0; index < selectedCellCount; index++)
                {
                    endRow = dgvExcel2.SelectedCells[index].RowIndex;
                    endColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                    if (biggestRow < endRow)
                        biggestRow = endRow;
                    if (biggestColumn < endColumn)
                        biggestColumn = endColumn;
                }
                endRow = biggestRow;
                endColumn = biggestColumn;
                for (int index = 0; index < selectedCellCount; index++)
                {
                    startRow = dgvExcel2.SelectedCells[index].RowIndex;
                    startColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                    if (smallestRow > startRow)
                        smallestRow = startRow;
                    if (smallestColumn > startColumn)
                        smallestColumn = startColumn;
                }
                startRow = smallestRow;
                startColumn = smallestColumn;
            }
            for (int Row = startRow; Row <= endRow; Row++)
            {
                for (int Column = startColumn; Column <= endColumn; Column++)
                {
                    excelArray2[currentPageNumber - 1, Row, Column] = "";
                }
            }
            DataTable dt = new DataTable();
            dt = Features_Algorithm.getExcelTable(excelArray2, cmbPage2.SelectedIndex);
            dgvExcel2.DataSource = dt.DefaultView;
        }

        private void btnChart_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath))
            {
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;

                Int32 selectedCellCount = dgvExcel.GetCellCount(DataGridViewElementStates.Selected);
                if (selectedCellCount > 0)
                {
                    int smallestRow = dgvExcel.SelectedCells[selectedCellCount - 1].RowIndex;
                    int smallestColumn = dgvExcel.SelectedCells[selectedCellCount - 1].ColumnIndex;
                    int biggestRow = 0;
                    int biggestColumn = 0;

                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        endRow = dgvExcel.SelectedCells[index].RowIndex;
                        endColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                        if (biggestRow < endRow)
                            biggestRow = endRow;
                        if (biggestColumn < endColumn)
                            biggestColumn = endColumn;
                    }
                    endRow = biggestRow;
                    endColumn = biggestColumn;
                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        startRow = dgvExcel.SelectedCells[index].RowIndex;
                        startColumn = dgvExcel.SelectedCells[index].ColumnIndex;
                        if (smallestRow > startRow)
                            smallestRow = startRow;
                        if (smallestColumn > startColumn)
                            smallestColumn = startColumn;
                    }
                    startRow = smallestRow;
                    startColumn = smallestColumn;

                    addChartButton(currentPageNumber, startRow, startColumn, endRow, endColumn);

                    EC.Add(new Excel_Chart()
                    {
                        page = currentPageNumber,
                        startColumn = startColumn,
                        startRow = startRow,
                        endColumn = endColumn,
                        endRow = endRow,
                        chartType = cmbChart.SelectedItem.ToString()
                    });

                    MessageBox.Show("Chart Add.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private void btnChart2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(excelPath2))
            {
                int startRow = 0;
                int startColumn = 0;
                int endRow = 0;
                int endColumn = 0;

                Int32 selectedCellCount = dgvExcel2.GetCellCount(DataGridViewElementStates.Selected);
                if (selectedCellCount > 0)
                {
                    int smallestRow = dgvExcel2.SelectedCells[selectedCellCount - 1].RowIndex;
                    int smallestColumn = dgvExcel2.SelectedCells[selectedCellCount - 1].ColumnIndex;
                    int biggestRow = 0;
                    int biggestColumn = 0;

                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        endRow = dgvExcel2.SelectedCells[index].RowIndex;
                        endColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                        if (biggestRow < endRow)
                            biggestRow = endRow;
                        if (biggestColumn < endColumn)
                            biggestColumn = endColumn;
                    }
                    endRow = biggestRow;
                    endColumn = biggestColumn;
                    for (int index = 0; index < selectedCellCount; index++)
                    {
                        startRow = dgvExcel2.SelectedCells[index].RowIndex;
                        startColumn = dgvExcel2.SelectedCells[index].ColumnIndex;
                        if (smallestRow > startRow)
                            smallestRow = startRow;
                        if (smallestColumn > startColumn)
                            smallestColumn = startColumn;
                    }
                    startRow = smallestRow;
                    startColumn = smallestColumn;

                    addChartButton2(currentPageNumber, startRow, startColumn, endRow, endColumn);

                    EC2.Add(new Excel_Chart()
                    {
                        page = currentPageNumber,
                        startColumn = startColumn,
                        startRow = startRow,
                        endColumn = endColumn,
                        endRow = endRow,
                        chartType = cmbChart2.SelectedItem.ToString()
                    });

                    MessageBox.Show("Chart Add.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private void addChartButton(int page,int sRow, int sColumn, int eRow, int eColumn)
        {
            tblChart.ColumnCount = tblChart.ColumnCount + 1;
            Button btn = chartButton( page,  sRow,  sColumn,  eRow,  eColumn,1);
            btn.Click += new System.EventHandler(this.deleteChart);
            tblChart.Controls.Add(btn, tblChart.ColumnCount-1 /* Column Index */, 0 /* Row index */);
            count++;
        }


        private void addChartButton2(int page, int sRow, int sColumn, int eRow, int eColumn)
        {
            tblChart2.ColumnCount = tblChart2.ColumnCount + 1;
            Button btn = chartButton(page, sRow, sColumn, eRow, eColumn, 2);
            btn.Click += new System.EventHandler(this.deleteChart2);
            tblChart2.Controls.Add(btn, tblChart2.ColumnCount - 1 /* Column Index */, 0 /* Row index */);
            count2++;
        }

        void deleteChart2(object sender, EventArgs e)
        {
            int page, countIndex;
            Button currentButton = (Button)sender;
            string[] btn = currentButton.Text.Split(':');
            page = int.Parse(btn[0].Substring(4));
            countIndex = int.Parse(btn[1]);

            if (MessageBox.Show("Delete page: " + page + " \nChart: " + (countIndex + 1), "Chart Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // EC.RemoveAt((EC.Count - 1 )- countIndex);
                EC2[countIndex].chartType = "None";
                //Form1.Controls.Remove(currentButton);
                currentButton.Dispose();
            }

        }
        void deleteChart(object sender, EventArgs e)
        {
            int page, countIndex;
            Button currentButton = (Button)sender;
            string[] btn = currentButton.Text.Split(':');
            page = int.Parse(btn[0].Substring(4));
            countIndex = int.Parse(btn[1]);

            if (MessageBox.Show("Delete page: " + page + " \nChart: " + (countIndex + 1), "Chart Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // EC.RemoveAt((EC.Count - 1 )- countIndex);
                EC[countIndex].chartType = "None";
                //Form1.Controls.Remove(currentButton);
                currentButton.Dispose();
            }

        }

        Button chartButton(int page, int sRow, int sColumn, int eRow, int eColumn, int dgvNum)
        {
            var btn = new Button();
            btn.Size = new Size(80, 26);
            btn.TextAlign = ContentAlignment.MiddleCenter;
            btn.Margin = new Padding(5);
            btn.BackColor = System.Drawing.Color.Red;
            btn.ForeColor = System.Drawing.Color.White;
            if(dgvNum==1)
                btn.Text = "Page" + page + ":" + (count);
            else
                btn.Text = "Page" + page + ":" + (count2);
            btn.Name = "btn" + btnIndex;
            btn.Cursor = Cursors.Default;
            btnIndex++;

            return btn;
        }
        
    }
}
