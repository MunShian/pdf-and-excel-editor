﻿namespace WindowPDFViewer.Excel_Editor
{
    partial class ExcelEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvExcel = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvExcel2 = new System.Windows.Forms.DataGridView();
            this.txtPath2 = new System.Windows.Forms.TextBox();
            this.cmbPage2 = new System.Windows.Forms.ComboBox();
            this.btnOpen2 = new System.Windows.Forms.Button();
            this.btnSaveAs2 = new System.Windows.Forms.Button();
            this.btnSave2 = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnPDFEditor = new System.Windows.Forms.Button();
            this.btnPaste2 = new System.Windows.Forms.Button();
            this.gbCalculate = new System.Windows.Forms.GroupBox();
            this.btnCopyTotal = new System.Windows.Forms.Button();
            this.dgvTotal = new System.Windows.Forms.DataGridView();
            this.btnTotal = new System.Windows.Forms.Button();
            this.btnPercent = new System.Windows.Forms.Button();
            this.btnCount = new System.Windows.Forms.Button();
            this.btnAverage = new System.Windows.Forms.Button();
            this.btnSum = new System.Windows.Forms.Button();
            this.gbFilter = new System.Windows.Forms.GroupBox();
            this.tblChart2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblType = new System.Windows.Forms.Label();
            this.gbHex = new System.Windows.Forms.GroupBox();
            this.btnDecToHex = new System.Windows.Forms.Button();
            this.btnHexToDec = new System.Windows.Forms.Button();
            this.gbBin = new System.Windows.Forms.GroupBox();
            this.btnBinToDec = new System.Windows.Forms.Button();
            this.btnDecToBin = new System.Windows.Forms.Button();
            this.gbOct = new System.Windows.Forms.GroupBox();
            this.btnDecToOct = new System.Windows.Forms.Button();
            this.btnOctToDec = new System.Windows.Forms.Button();
            this.btnCopyFilter = new System.Windows.Forms.Button();
            this.dgvFilter = new System.Windows.Forms.DataGridView();
            this.btnNumeric = new System.Windows.Forms.Button();
            this.btnCharacter = new System.Windows.Forms.Button();
            this.btnPaste = new System.Windows.Forms.Button();
            this.btnCopy2 = new System.Windows.Forms.Button();
            this.btnChart = new System.Windows.Forms.Button();
            this.cmbChart = new System.Windows.Forms.ComboBox();
            this.tblChart = new System.Windows.Forms.TableLayoutPanel();
            this.cmbChart2 = new System.Windows.Forms.ComboBox();
            this.btnChart2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcel)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcel2)).BeginInit();
            this.gbCalculate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).BeginInit();
            this.gbFilter.SuspendLayout();
            this.gbHex.SuspendLayout();
            this.gbBin.SuspendLayout();
            this.gbOct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbPage
            // 
            this.cmbPage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPage.FormattingEnabled = true;
            this.cmbPage.Location = new System.Drawing.Point(741, 31);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(95, 28);
            this.cmbPage.TabIndex = 8;
            this.cmbPage.SelectedIndexChanged += new System.EventHandler(this.cmbPage_SelectedIndexChanged);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(634, 31);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(101, 33);
            this.btnOpen.TabIndex = 7;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(6, 31);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(622, 26);
            this.txtPath.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvExcel);
            this.groupBox1.Controls.Add(this.txtPath);
            this.groupBox1.Controls.Add(this.cmbPage);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(900, 651);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Excel 1";
            // 
            // dgvExcel
            // 
            this.dgvExcel.AllowUserToAddRows = false;
            this.dgvExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvExcel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExcel.Location = new System.Drawing.Point(6, 72);
            this.dgvExcel.Name = "dgvExcel";
            this.dgvExcel.RowTemplate.Height = 28;
            this.dgvExcel.Size = new System.Drawing.Size(888, 573);
            this.dgvExcel.TabIndex = 9;
            this.dgvExcel.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dgvExcel_CellContextMenuStripNeeded);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(238, 669);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(104, 37);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(348, 669);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(104, 37);
            this.btnSaveAs.TabIndex = 11;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.SaveAs_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvExcel2);
            this.groupBox2.Controls.Add(this.txtPath2);
            this.groupBox2.Controls.Add(this.cmbPage2);
            this.groupBox2.Controls.Add(this.btnOpen2);
            this.groupBox2.Location = new System.Drawing.Point(918, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(900, 651);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Excel 2";
            // 
            // dgvExcel2
            // 
            this.dgvExcel2.AllowUserToAddRows = false;
            this.dgvExcel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvExcel2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExcel2.Location = new System.Drawing.Point(6, 72);
            this.dgvExcel2.Name = "dgvExcel2";
            this.dgvExcel2.RowTemplate.Height = 28;
            this.dgvExcel2.Size = new System.Drawing.Size(888, 573);
            this.dgvExcel2.TabIndex = 9;
            this.dgvExcel2.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dgvExcel2_CellContextMenuStripNeeded);
            // 
            // txtPath2
            // 
            this.txtPath2.Location = new System.Drawing.Point(6, 31);
            this.txtPath2.Name = "txtPath2";
            this.txtPath2.ReadOnly = true;
            this.txtPath2.Size = new System.Drawing.Size(622, 26);
            this.txtPath2.TabIndex = 6;
            // 
            // cmbPage2
            // 
            this.cmbPage2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPage2.FormattingEnabled = true;
            this.cmbPage2.Location = new System.Drawing.Point(741, 31);
            this.cmbPage2.Name = "cmbPage2";
            this.cmbPage2.Size = new System.Drawing.Size(95, 28);
            this.cmbPage2.TabIndex = 8;
            this.cmbPage2.SelectedIndexChanged += new System.EventHandler(this.cmbPage2_SelectedIndexChanged);
            // 
            // btnOpen2
            // 
            this.btnOpen2.Location = new System.Drawing.Point(634, 31);
            this.btnOpen2.Name = "btnOpen2";
            this.btnOpen2.Size = new System.Drawing.Size(101, 33);
            this.btnOpen2.TabIndex = 7;
            this.btnOpen2.Text = "Open";
            this.btnOpen2.UseVisualStyleBackColor = true;
            this.btnOpen2.Click += new System.EventHandler(this.btnOpen2_Click);
            // 
            // btnSaveAs2
            // 
            this.btnSaveAs2.Location = new System.Drawing.Point(1254, 669);
            this.btnSaveAs2.Name = "btnSaveAs2";
            this.btnSaveAs2.Size = new System.Drawing.Size(104, 37);
            this.btnSaveAs2.TabIndex = 14;
            this.btnSaveAs2.Text = "Save As";
            this.btnSaveAs2.UseVisualStyleBackColor = true;
            this.btnSaveAs2.Click += new System.EventHandler(this.btnSaveAs2_Click);
            // 
            // btnSave2
            // 
            this.btnSave2.Location = new System.Drawing.Point(1144, 669);
            this.btnSave2.Name = "btnSave2";
            this.btnSave2.Size = new System.Drawing.Size(104, 37);
            this.btnSave2.TabIndex = 13;
            this.btnSave2.Text = "Save";
            this.btnSave2.UseVisualStyleBackColor = true;
            this.btnSave2.Click += new System.EventHandler(this.btnSave2_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(18, 669);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(104, 37);
            this.btnCopy.TabIndex = 15;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnPDFEditor
            // 
            this.btnPDFEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPDFEditor.Location = new System.Drawing.Point(12, 964);
            this.btnPDFEditor.Name = "btnPDFEditor";
            this.btnPDFEditor.Size = new System.Drawing.Size(123, 43);
            this.btnPDFEditor.TabIndex = 16;
            this.btnPDFEditor.Text = "PDF Editor";
            this.btnPDFEditor.UseVisualStyleBackColor = true;
            this.btnPDFEditor.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // btnPaste2
            // 
            this.btnPaste2.Location = new System.Drawing.Point(1034, 669);
            this.btnPaste2.Name = "btnPaste2";
            this.btnPaste2.Size = new System.Drawing.Size(104, 37);
            this.btnPaste2.TabIndex = 17;
            this.btnPaste2.Text = "Paste";
            this.btnPaste2.UseVisualStyleBackColor = true;
            this.btnPaste2.Click += new System.EventHandler(this.btnPaste2_Click);
            // 
            // gbCalculate
            // 
            this.gbCalculate.Controls.Add(this.btnCopyTotal);
            this.gbCalculate.Controls.Add(this.dgvTotal);
            this.gbCalculate.Controls.Add(this.btnTotal);
            this.gbCalculate.Controls.Add(this.btnPercent);
            this.gbCalculate.Controls.Add(this.btnCount);
            this.gbCalculate.Controls.Add(this.btnAverage);
            this.gbCalculate.Controls.Add(this.btnSum);
            this.gbCalculate.Location = new System.Drawing.Point(18, 712);
            this.gbCalculate.Name = "gbCalculate";
            this.gbCalculate.Size = new System.Drawing.Size(888, 216);
            this.gbCalculate.TabIndex = 18;
            this.gbCalculate.TabStop = false;
            this.gbCalculate.Text = "Calculation";
            // 
            // btnCopyTotal
            // 
            this.btnCopyTotal.Location = new System.Drawing.Point(784, 173);
            this.btnCopyTotal.Name = "btnCopyTotal";
            this.btnCopyTotal.Size = new System.Drawing.Size(104, 37);
            this.btnCopyTotal.TabIndex = 17;
            this.btnCopyTotal.Text = "Copy";
            this.btnCopyTotal.UseVisualStyleBackColor = true;
            this.btnCopyTotal.Click += new System.EventHandler(this.btnCopyTotal_Click);
            // 
            // dgvTotal
            // 
            this.dgvTotal.AllowUserToAddRows = false;
            this.dgvTotal.AllowUserToDeleteRows = false;
            this.dgvTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTotal.ColumnHeadersVisible = false;
            this.dgvTotal.Location = new System.Drawing.Point(6, 68);
            this.dgvTotal.Name = "dgvTotal";
            this.dgvTotal.ReadOnly = true;
            this.dgvTotal.RowTemplate.Height = 28;
            this.dgvTotal.Size = new System.Drawing.Size(772, 142);
            this.dgvTotal.TabIndex = 16;
            // 
            // btnTotal
            // 
            this.btnTotal.Location = new System.Drawing.Point(446, 25);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(138, 37);
            this.btnTotal.TabIndex = 15;
            this.btnTotal.Text = "Running Total";
            this.btnTotal.UseVisualStyleBackColor = true;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            // 
            // btnPercent
            // 
            this.btnPercent.Location = new System.Drawing.Point(336, 25);
            this.btnPercent.Name = "btnPercent";
            this.btnPercent.Size = new System.Drawing.Size(104, 37);
            this.btnPercent.TabIndex = 14;
            this.btnPercent.Text = "% Total";
            this.btnPercent.UseVisualStyleBackColor = true;
            this.btnPercent.Click += new System.EventHandler(this.btnPercent_Click);
            // 
            // btnCount
            // 
            this.btnCount.Location = new System.Drawing.Point(226, 25);
            this.btnCount.Name = "btnCount";
            this.btnCount.Size = new System.Drawing.Size(104, 37);
            this.btnCount.TabIndex = 13;
            this.btnCount.Text = "Count";
            this.btnCount.UseVisualStyleBackColor = true;
            this.btnCount.Click += new System.EventHandler(this.btnCount_Click);
            // 
            // btnAverage
            // 
            this.btnAverage.Location = new System.Drawing.Point(116, 25);
            this.btnAverage.Name = "btnAverage";
            this.btnAverage.Size = new System.Drawing.Size(104, 37);
            this.btnAverage.TabIndex = 12;
            this.btnAverage.Text = "Average";
            this.btnAverage.UseVisualStyleBackColor = true;
            this.btnAverage.Click += new System.EventHandler(this.btnAverage_Click);
            // 
            // btnSum
            // 
            this.btnSum.Location = new System.Drawing.Point(6, 25);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(104, 37);
            this.btnSum.TabIndex = 11;
            this.btnSum.Text = "Sum";
            this.btnSum.UseVisualStyleBackColor = true;
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.tblChart2);
            this.gbFilter.Controls.Add(this.lblType);
            this.gbFilter.Controls.Add(this.gbHex);
            this.gbFilter.Controls.Add(this.gbBin);
            this.gbFilter.Controls.Add(this.gbOct);
            this.gbFilter.Controls.Add(this.btnCopyFilter);
            this.gbFilter.Controls.Add(this.dgvFilter);
            this.gbFilter.Controls.Add(this.btnNumeric);
            this.gbFilter.Controls.Add(this.btnCharacter);
            this.gbFilter.Location = new System.Drawing.Point(912, 712);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(900, 295);
            this.gbFilter.TabIndex = 19;
            this.gbFilter.TabStop = false;
            this.gbFilter.Text = "Filter and Convert";
            // 
            // tblChart2
            // 
            this.tblChart2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tblChart2.AutoScroll = true;
            this.tblChart2.ColumnCount = 1;
            this.tblChart2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblChart2.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tblChart2.Location = new System.Drawing.Point(6, 222);
            this.tblChart2.Name = "tblChart2";
            this.tblChart2.RowCount = 1;
            this.tblChart2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblChart2.Size = new System.Drawing.Size(772, 73);
            this.tblChart2.TabIndex = 28;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(7, 45);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(89, 25);
            this.lblType.TabIndex = 27;
            this.lblType.Text = "Decimal";
            // 
            // gbHex
            // 
            this.gbHex.Controls.Add(this.btnDecToHex);
            this.gbHex.Controls.Add(this.btnHexToDec);
            this.gbHex.Location = new System.Drawing.Point(633, 20);
            this.gbHex.Name = "gbHex";
            this.gbHex.Size = new System.Drawing.Size(254, 64);
            this.gbHex.TabIndex = 26;
            this.gbHex.TabStop = false;
            this.gbHex.Text = "HexaDecimal ";
            // 
            // btnDecToHex
            // 
            this.btnDecToHex.Location = new System.Drawing.Point(6, 21);
            this.btnDecToHex.Name = "btnDecToHex";
            this.btnDecToHex.Size = new System.Drawing.Size(127, 37);
            this.btnDecToHex.TabIndex = 20;
            this.btnDecToHex.Text = "Dec to Hex";
            this.btnDecToHex.UseVisualStyleBackColor = true;
            this.btnDecToHex.Click += new System.EventHandler(this.btnDecToHex_Click);
            // 
            // btnHexToDec
            // 
            this.btnHexToDec.Location = new System.Drawing.Point(139, 21);
            this.btnHexToDec.Name = "btnHexToDec";
            this.btnHexToDec.Size = new System.Drawing.Size(109, 37);
            this.btnHexToDec.TabIndex = 21;
            this.btnHexToDec.Text = "Hex to Dec";
            this.btnHexToDec.UseVisualStyleBackColor = true;
            this.btnHexToDec.Click += new System.EventHandler(this.btnHexToDec_Click);
            // 
            // gbBin
            // 
            this.gbBin.Controls.Add(this.btnBinToDec);
            this.gbBin.Controls.Add(this.btnDecToBin);
            this.gbBin.Location = new System.Drawing.Point(160, 20);
            this.gbBin.Name = "gbBin";
            this.gbBin.Size = new System.Drawing.Size(214, 64);
            this.gbBin.TabIndex = 25;
            this.gbBin.TabStop = false;
            this.gbBin.Text = "Binary";
            // 
            // btnBinToDec
            // 
            this.btnBinToDec.Location = new System.Drawing.Point(109, 21);
            this.btnBinToDec.Name = "btnBinToDec";
            this.btnBinToDec.Size = new System.Drawing.Size(97, 37);
            this.btnBinToDec.TabIndex = 23;
            this.btnBinToDec.Text = "Bin to Dec";
            this.btnBinToDec.UseVisualStyleBackColor = true;
            this.btnBinToDec.Click += new System.EventHandler(this.btnBinToDec_Click);
            // 
            // btnDecToBin
            // 
            this.btnDecToBin.Location = new System.Drawing.Point(6, 21);
            this.btnDecToBin.Name = "btnDecToBin";
            this.btnDecToBin.Size = new System.Drawing.Size(97, 37);
            this.btnDecToBin.TabIndex = 22;
            this.btnDecToBin.Text = "Dec to Bin";
            this.btnDecToBin.UseVisualStyleBackColor = true;
            this.btnDecToBin.Click += new System.EventHandler(this.btnDecToBin_Click);
            // 
            // gbOct
            // 
            this.gbOct.Controls.Add(this.btnDecToOct);
            this.gbOct.Controls.Add(this.btnOctToDec);
            this.gbOct.Location = new System.Drawing.Point(380, 20);
            this.gbOct.Name = "gbOct";
            this.gbOct.Size = new System.Drawing.Size(247, 64);
            this.gbOct.TabIndex = 24;
            this.gbOct.TabStop = false;
            this.gbOct.Text = "Octal";
            // 
            // btnDecToOct
            // 
            this.btnDecToOct.Location = new System.Drawing.Point(6, 21);
            this.btnDecToOct.Name = "btnDecToOct";
            this.btnDecToOct.Size = new System.Drawing.Size(115, 37);
            this.btnDecToOct.TabIndex = 25;
            this.btnDecToOct.Text = "Dec to Oct";
            this.btnDecToOct.UseVisualStyleBackColor = true;
            this.btnDecToOct.Click += new System.EventHandler(this.btnDecToOct_Click);
            // 
            // btnOctToDec
            // 
            this.btnOctToDec.Location = new System.Drawing.Point(127, 21);
            this.btnOctToDec.Name = "btnOctToDec";
            this.btnOctToDec.Size = new System.Drawing.Size(114, 37);
            this.btnOctToDec.TabIndex = 24;
            this.btnOctToDec.Text = "Oct to Dec";
            this.btnOctToDec.UseVisualStyleBackColor = true;
            this.btnOctToDec.Click += new System.EventHandler(this.btnOctToDec_Click);
            // 
            // btnCopyFilter
            // 
            this.btnCopyFilter.Location = new System.Drawing.Point(784, 210);
            this.btnCopyFilter.Name = "btnCopyFilter";
            this.btnCopyFilter.Size = new System.Drawing.Size(104, 37);
            this.btnCopyFilter.TabIndex = 19;
            this.btnCopyFilter.Text = "Copy";
            this.btnCopyFilter.UseVisualStyleBackColor = true;
            this.btnCopyFilter.Click += new System.EventHandler(this.btnCopyFilter_Click);
            // 
            // dgvFilter
            // 
            this.dgvFilter.AllowUserToAddRows = false;
            this.dgvFilter.AllowUserToDeleteRows = false;
            this.dgvFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilter.ColumnHeadersVisible = false;
            this.dgvFilter.Location = new System.Drawing.Point(6, 90);
            this.dgvFilter.Name = "dgvFilter";
            this.dgvFilter.ReadOnly = true;
            this.dgvFilter.RowTemplate.Height = 28;
            this.dgvFilter.Size = new System.Drawing.Size(772, 126);
            this.dgvFilter.TabIndex = 18;
            // 
            // btnNumeric
            // 
            this.btnNumeric.Location = new System.Drawing.Point(784, 147);
            this.btnNumeric.Name = "btnNumeric";
            this.btnNumeric.Size = new System.Drawing.Size(103, 57);
            this.btnNumeric.TabIndex = 17;
            this.btnNumeric.Text = "Numeric Only";
            this.btnNumeric.UseVisualStyleBackColor = true;
            this.btnNumeric.Click += new System.EventHandler(this.btnNumeric_Click);
            // 
            // btnCharacter
            // 
            this.btnCharacter.Location = new System.Drawing.Point(784, 84);
            this.btnCharacter.Name = "btnCharacter";
            this.btnCharacter.Size = new System.Drawing.Size(104, 57);
            this.btnCharacter.TabIndex = 16;
            this.btnCharacter.Text = "Character Only";
            this.btnCharacter.UseVisualStyleBackColor = true;
            this.btnCharacter.Click += new System.EventHandler(this.btnCharacter_Click);
            // 
            // btnPaste
            // 
            this.btnPaste.Location = new System.Drawing.Point(128, 669);
            this.btnPaste.Name = "btnPaste";
            this.btnPaste.Size = new System.Drawing.Size(104, 37);
            this.btnPaste.TabIndex = 20;
            this.btnPaste.Text = "Paste";
            this.btnPaste.UseVisualStyleBackColor = true;
            this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
            // 
            // btnCopy2
            // 
            this.btnCopy2.Location = new System.Drawing.Point(924, 669);
            this.btnCopy2.Name = "btnCopy2";
            this.btnCopy2.Size = new System.Drawing.Size(104, 37);
            this.btnCopy2.TabIndex = 21;
            this.btnCopy2.Text = "Copy";
            this.btnCopy2.UseVisualStyleBackColor = true;
            this.btnCopy2.Click += new System.EventHandler(this.btnCopy2_Click);
            // 
            // btnChart
            // 
            this.btnChart.Location = new System.Drawing.Point(458, 669);
            this.btnChart.Name = "btnChart";
            this.btnChart.Size = new System.Drawing.Size(104, 37);
            this.btnChart.TabIndex = 22;
            this.btnChart.Text = "Chart";
            this.btnChart.UseVisualStyleBackColor = true;
            this.btnChart.Click += new System.EventHandler(this.btnChart_Click);
            // 
            // cmbChart
            // 
            this.cmbChart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChart.FormattingEnabled = true;
            this.cmbChart.Location = new System.Drawing.Point(664, 671);
            this.cmbChart.Name = "cmbChart";
            this.cmbChart.Size = new System.Drawing.Size(179, 28);
            this.cmbChart.TabIndex = 23;
            // 
            // tblChart
            // 
            this.tblChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tblChart.AutoScroll = true;
            this.tblChart.ColumnCount = 1;
            this.tblChart.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblChart.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tblChart.Location = new System.Drawing.Point(141, 934);
            this.tblChart.Name = "tblChart";
            this.tblChart.RowCount = 1;
            this.tblChart.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblChart.Size = new System.Drawing.Size(655, 73);
            this.tblChart.TabIndex = 24;
            // 
            // cmbChart2
            // 
            this.cmbChart2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChart2.FormattingEnabled = true;
            this.cmbChart2.Location = new System.Drawing.Point(1570, 669);
            this.cmbChart2.Name = "cmbChart2";
            this.cmbChart2.Size = new System.Drawing.Size(179, 28);
            this.cmbChart2.TabIndex = 26;
            // 
            // btnChart2
            // 
            this.btnChart2.Location = new System.Drawing.Point(1364, 670);
            this.btnChart2.Name = "btnChart2";
            this.btnChart2.Size = new System.Drawing.Size(104, 37);
            this.btnChart2.TabIndex = 25;
            this.btnChart2.Text = "Chart";
            this.btnChart2.UseVisualStyleBackColor = true;
            this.btnChart2.Click += new System.EventHandler(this.btnChart2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(568, 677);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Chart Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1474, 677);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Chart Type:";
            // 
            // ExcelEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1825, 1019);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbChart2);
            this.Controls.Add(this.btnChart2);
            this.Controls.Add(this.tblChart);
            this.Controls.Add(this.cmbChart);
            this.Controls.Add(this.btnChart);
            this.Controls.Add(this.btnCopy2);
            this.Controls.Add(this.btnPaste);
            this.Controls.Add(this.gbFilter);
            this.Controls.Add(this.gbCalculate);
            this.Controls.Add(this.btnPaste2);
            this.Controls.Add(this.btnPDFEditor);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnSaveAs2);
            this.Controls.Add(this.btnSave2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox1);
            this.Name = "ExcelEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExcelEditor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcel)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcel2)).EndInit();
            this.gbCalculate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).EndInit();
            this.gbFilter.ResumeLayout(false);
            this.gbFilter.PerformLayout();
            this.gbHex.ResumeLayout(false);
            this.gbBin.ResumeLayout(false);
            this.gbOct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvExcel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvExcel2;
        private System.Windows.Forms.TextBox txtPath2;
        private System.Windows.Forms.ComboBox cmbPage2;
        private System.Windows.Forms.Button btnOpen2;
        private System.Windows.Forms.Button btnSaveAs2;
        private System.Windows.Forms.Button btnSave2;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnPDFEditor;
        private System.Windows.Forms.Button btnPaste2;
        private System.Windows.Forms.GroupBox gbCalculate;
        private System.Windows.Forms.Button btnCount;
        private System.Windows.Forms.Button btnAverage;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.Button btnPercent;
        private System.Windows.Forms.DataGridView dgvTotal;
        private System.Windows.Forms.Button btnCopyTotal;
        private System.Windows.Forms.GroupBox gbFilter;
        private System.Windows.Forms.Button btnNumeric;
        private System.Windows.Forms.Button btnCharacter;
        private System.Windows.Forms.Button btnCopyFilter;
        private System.Windows.Forms.DataGridView dgvFilter;
        private System.Windows.Forms.Button btnPaste;
        private System.Windows.Forms.Button btnCopy2;
        private System.Windows.Forms.GroupBox gbHex;
        private System.Windows.Forms.Button btnDecToHex;
        private System.Windows.Forms.Button btnHexToDec;
        private System.Windows.Forms.GroupBox gbBin;
        private System.Windows.Forms.Button btnBinToDec;
        private System.Windows.Forms.Button btnDecToBin;
        private System.Windows.Forms.GroupBox gbOct;
        private System.Windows.Forms.Button btnDecToOct;
        private System.Windows.Forms.Button btnOctToDec;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnChart;
        private System.Windows.Forms.ComboBox cmbChart;
        private System.Windows.Forms.TableLayoutPanel tblChart;
        private System.Windows.Forms.TableLayoutPanel tblChart2;
        private System.Windows.Forms.ComboBox cmbChart2;
        private System.Windows.Forms.Button btnChart2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}