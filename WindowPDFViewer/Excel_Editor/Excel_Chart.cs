﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowPDFViewer.Excel_Editor
{
    public class Excel_Chart
    {
        public int page { get; set; }
        public int startColumn { get; set; }
        public int startRow { get; set; }
        public int endColumn { get; set; }
        public int endRow { get; set; }
        public string chartType { get; set; }
        //public int page2 { get; set; }
        //public int startColumn2 { get; set; }
        //public int startRow2 { get; set; }
        //public int endColumn2 { get; set; }
        //public int endRow2 { get; set; }
        //public string chartType2 { get; set; }

        //public int chartNumber { get; set; }
    }
}
