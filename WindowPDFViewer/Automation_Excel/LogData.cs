﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowPDFViewer.Automation_Excel
{
    public class LogData
    {
        public DateTime dateTime { get; set;}
        public string targetSheet { get; set;}
        public string changedRange { get; set;}
        public string action { get; set; }
        public string value { get; set; }
    }
}
