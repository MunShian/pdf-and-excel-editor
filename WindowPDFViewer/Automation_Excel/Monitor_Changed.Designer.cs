﻿namespace WindowPDFViewer.Automation_Excel
{
    partial class Monitor_Changed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbMonitor = new System.Windows.Forms.RichTextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnBGExcel = new System.Windows.Forms.Button();
            this.lblLogData = new System.Windows.Forms.Label();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbTrack = new System.Windows.Forms.GroupBox();
            this.lblOptional = new System.Windows.Forms.Label();
            this.lblVertical2 = new System.Windows.Forms.Label();
            this.lblHorizontal2 = new System.Windows.Forms.Label();
            this.txtHori2 = new System.Windows.Forms.TextBox();
            this.txtVert2 = new System.Windows.Forms.TextBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lbTrack = new System.Windows.Forms.ListBox();
            this.btnTrack = new System.Windows.Forms.Button();
            this.chkTrackAll = new System.Windows.Forms.CheckBox();
            this.lblVertical = new System.Windows.Forms.Label();
            this.lblHorizontal = new System.Windows.Forms.Label();
            this.txtHori = new System.Windows.Forms.TextBox();
            this.txtVert = new System.Windows.Forms.TextBox();
            this.gbUploadDatabase = new System.Windows.Forms.GroupBox();
            this.chkChangedUpload = new System.Windows.Forms.CheckBox();
            this.lblUploadTime = new System.Windows.Forms.Label();
            this.btnStopTimer = new System.Windows.Forms.Button();
            this.btnSecond = new System.Windows.Forms.Label();
            this.txtFreqSec = new System.Windows.Forms.TextBox();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.btnUploadDB = new System.Windows.Forms.Button();
            this.gbProcedure = new System.Windows.Forms.GroupBox();
            this.btnRmvProc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProcedureName = new System.Windows.Forms.TextBox();
            this.lbProcedure = new System.Windows.Forms.ListBox();
            this.lblProcedure = new System.Windows.Forms.Label();
            this.txtProcedure = new System.Windows.Forms.TextBox();
            this.btnAddCells = new System.Windows.Forms.Button();
            this.txtPHori = new System.Windows.Forms.TextBox();
            this.txtPVert = new System.Windows.Forms.TextBox();
            this.btnAddProcess = new System.Windows.Forms.Button();
            this.lblPHori = new System.Windows.Forms.Label();
            this.btnProcedure = new System.Windows.Forms.Button();
            this.lblPVert = new System.Windows.Forms.Label();
            this.lbProcess = new System.Windows.Forms.ListBox();
            this.btnRunProc = new System.Windows.Forms.Button();
            this.dgvOutput = new System.Windows.Forms.DataGridView();
            this.lblOutput = new System.Windows.Forms.Label();
            this.txtPHori2 = new System.Windows.Forms.TextBox();
            this.txtPVert2 = new System.Windows.Forms.TextBox();
            this.lblPHori2 = new System.Windows.Forms.Label();
            this.lblPVert2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbTrack.SuspendLayout();
            this.gbUploadDatabase.SuspendLayout();
            this.gbProcedure.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutput)).BeginInit();
            this.SuspendLayout();
            // 
            // rtbMonitor
            // 
            this.rtbMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbMonitor.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.rtbMonitor.Location = new System.Drawing.Point(12, 301);
            this.rtbMonitor.Name = "rtbMonitor";
            this.rtbMonitor.ReadOnly = true;
            this.rtbMonitor.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbMonitor.Size = new System.Drawing.Size(1190, 522);
            this.rtbMonitor.TabIndex = 0;
            this.rtbMonitor.Text = "";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(12, 12);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(622, 26);
            this.txtPath.TabIndex = 8;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(640, 9);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(101, 33);
            this.btnOpen.TabIndex = 9;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnBGExcel
            // 
            this.btnBGExcel.Location = new System.Drawing.Point(747, 9);
            this.btnBGExcel.Name = "btnBGExcel";
            this.btnBGExcel.Size = new System.Drawing.Size(162, 33);
            this.btnBGExcel.TabIndex = 10;
            this.btnBGExcel.Text = "Background Excel";
            this.btnBGExcel.UseVisualStyleBackColor = true;
            this.btnBGExcel.Click += new System.EventHandler(this.btnBGExcel_Click);
            // 
            // lblLogData
            // 
            this.lblLogData.AutoSize = true;
            this.lblLogData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogData.Location = new System.Drawing.Point(13, 233);
            this.lblLogData.Name = "lblLogData";
            this.lblLogData.Size = new System.Drawing.Size(117, 29);
            this.lblLogData.TabIndex = 11;
            this.lblLogData.Text = "Log Data";
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(13, 265);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.ReadOnly = true;
            this.txtSavePath.Size = new System.Drawing.Size(622, 26);
            this.txtSavePath.TabIndex = 12;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(641, 262);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(101, 33);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Save Log";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbTrack
            // 
            this.gbTrack.Controls.Add(this.lblOptional);
            this.gbTrack.Controls.Add(this.lblVertical2);
            this.gbTrack.Controls.Add(this.lblHorizontal2);
            this.gbTrack.Controls.Add(this.txtHori2);
            this.gbTrack.Controls.Add(this.txtVert2);
            this.gbTrack.Controls.Add(this.btnRemove);
            this.gbTrack.Controls.Add(this.lbTrack);
            this.gbTrack.Controls.Add(this.btnTrack);
            this.gbTrack.Controls.Add(this.chkTrackAll);
            this.gbTrack.Controls.Add(this.lblVertical);
            this.gbTrack.Controls.Add(this.lblHorizontal);
            this.gbTrack.Controls.Add(this.txtHori);
            this.gbTrack.Controls.Add(this.txtVert);
            this.gbTrack.Location = new System.Drawing.Point(12, 44);
            this.gbTrack.Name = "gbTrack";
            this.gbTrack.Size = new System.Drawing.Size(736, 186);
            this.gbTrack.TabIndex = 15;
            this.gbTrack.TabStop = false;
            this.gbTrack.Text = "Track";
            // 
            // lblOptional
            // 
            this.lblOptional.AutoSize = true;
            this.lblOptional.Location = new System.Drawing.Point(12, 84);
            this.lblOptional.Name = "lblOptional";
            this.lblOptional.Size = new System.Drawing.Size(116, 20);
            this.lblOptional.TabIndex = 32;
            this.lblOptional.Text = "Optional*      To";
            // 
            // lblVertical2
            // 
            this.lblVertical2.AutoSize = true;
            this.lblVertical2.Location = new System.Drawing.Point(112, 116);
            this.lblVertical2.Name = "lblVertical2";
            this.lblVertical2.Size = new System.Drawing.Size(62, 20);
            this.lblVertical2.TabIndex = 31;
            this.lblVertical2.Text = "Vertical";
            // 
            // lblHorizontal2
            // 
            this.lblHorizontal2.AutoSize = true;
            this.lblHorizontal2.Location = new System.Drawing.Point(12, 116);
            this.lblHorizontal2.Name = "lblHorizontal2";
            this.lblHorizontal2.Size = new System.Drawing.Size(81, 20);
            this.lblHorizontal2.TabIndex = 30;
            this.lblHorizontal2.Text = "Horizontal";
            // 
            // txtHori2
            // 
            this.txtHori2.Location = new System.Drawing.Point(16, 139);
            this.txtHori2.Name = "txtHori2";
            this.txtHori2.Size = new System.Drawing.Size(94, 26);
            this.txtHori2.TabIndex = 29;
            // 
            // txtVert2
            // 
            this.txtVert2.Location = new System.Drawing.Point(116, 139);
            this.txtVert2.Name = "txtVert2";
            this.txtVert2.Size = new System.Drawing.Size(94, 26);
            this.txtVert2.TabIndex = 30;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(542, 139);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(92, 34);
            this.btnRemove.TabIndex = 27;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lbTrack
            // 
            this.lbTrack.FormattingEnabled = true;
            this.lbTrack.ItemHeight = 20;
            this.lbTrack.Location = new System.Drawing.Point(314, 26);
            this.lbTrack.Name = "lbTrack";
            this.lbTrack.ScrollAlwaysVisible = true;
            this.lbTrack.Size = new System.Drawing.Size(222, 144);
            this.lbTrack.TabIndex = 26;
            // 
            // btnTrack
            // 
            this.btnTrack.Location = new System.Drawing.Point(216, 45);
            this.btnTrack.Name = "btnTrack";
            this.btnTrack.Size = new System.Drawing.Size(92, 34);
            this.btnTrack.TabIndex = 19;
            this.btnTrack.Text = "Add Track";
            this.btnTrack.UseVisualStyleBackColor = true;
            this.btnTrack.Click += new System.EventHandler(this.btnTrack_Click);
            // 
            // chkTrackAll
            // 
            this.chkTrackAll.AutoSize = true;
            this.chkTrackAll.Location = new System.Drawing.Point(542, 109);
            this.chkTrackAll.Name = "chkTrackAll";
            this.chkTrackAll.Size = new System.Drawing.Size(194, 24);
            this.chkTrackAll.TabIndex = 24;
            this.chkTrackAll.Text = "Track all cells changed";
            this.chkTrackAll.UseVisualStyleBackColor = true;
            this.chkTrackAll.CheckedChanged += new System.EventHandler(this.chkTrackAll_CheckedChanged);
            // 
            // lblVertical
            // 
            this.lblVertical.AutoSize = true;
            this.lblVertical.Location = new System.Drawing.Point(112, 26);
            this.lblVertical.Name = "lblVertical";
            this.lblVertical.Size = new System.Drawing.Size(62, 20);
            this.lblVertical.TabIndex = 20;
            this.lblVertical.Text = "Vertical";
            // 
            // lblHorizontal
            // 
            this.lblHorizontal.AutoSize = true;
            this.lblHorizontal.Location = new System.Drawing.Point(12, 26);
            this.lblHorizontal.Name = "lblHorizontal";
            this.lblHorizontal.Size = new System.Drawing.Size(81, 20);
            this.lblHorizontal.TabIndex = 19;
            this.lblHorizontal.Text = "Horizontal";
            // 
            // txtHori
            // 
            this.txtHori.Location = new System.Drawing.Point(16, 49);
            this.txtHori.Name = "txtHori";
            this.txtHori.Size = new System.Drawing.Size(94, 26);
            this.txtHori.TabIndex = 17;
            // 
            // txtVert
            // 
            this.txtVert.Location = new System.Drawing.Point(116, 49);
            this.txtVert.Name = "txtVert";
            this.txtVert.Size = new System.Drawing.Size(94, 26);
            this.txtVert.TabIndex = 18;
            // 
            // gbUploadDatabase
            // 
            this.gbUploadDatabase.Controls.Add(this.chkChangedUpload);
            this.gbUploadDatabase.Controls.Add(this.lblUploadTime);
            this.gbUploadDatabase.Controls.Add(this.btnStopTimer);
            this.gbUploadDatabase.Controls.Add(this.btnSecond);
            this.gbUploadDatabase.Controls.Add(this.txtFreqSec);
            this.gbUploadDatabase.Controls.Add(this.lblFrequency);
            this.gbUploadDatabase.Controls.Add(this.btnUploadDB);
            this.gbUploadDatabase.Location = new System.Drawing.Point(755, 49);
            this.gbUploadDatabase.Name = "gbUploadDatabase";
            this.gbUploadDatabase.Size = new System.Drawing.Size(453, 246);
            this.gbUploadDatabase.TabIndex = 17;
            this.gbUploadDatabase.TabStop = false;
            this.gbUploadDatabase.Text = "Upload Database";
            // 
            // chkChangedUpload
            // 
            this.chkChangedUpload.AutoSize = true;
            this.chkChangedUpload.Location = new System.Drawing.Point(10, 218);
            this.chkChangedUpload.Name = "chkChangedUpload";
            this.chkChangedUpload.Size = new System.Drawing.Size(188, 24);
            this.chkChangedUpload.TabIndex = 34;
            this.chkChangedUpload.Text = "while changed upload";
            this.chkChangedUpload.UseVisualStyleBackColor = true;
            // 
            // lblUploadTime
            // 
            this.lblUploadTime.AutoSize = true;
            this.lblUploadTime.Location = new System.Drawing.Point(6, 22);
            this.lblUploadTime.Name = "lblUploadTime";
            this.lblUploadTime.Size = new System.Drawing.Size(133, 20);
            this.lblUploadTime.TabIndex = 33;
            this.lblUploadTime.Text = "Upload Log Time:";
            // 
            // btnStopTimer
            // 
            this.btnStopTimer.Location = new System.Drawing.Point(355, 206);
            this.btnStopTimer.Name = "btnStopTimer";
            this.btnStopTimer.Size = new System.Drawing.Size(92, 34);
            this.btnStopTimer.TabIndex = 32;
            this.btnStopTimer.Text = "Stop";
            this.btnStopTimer.UseVisualStyleBackColor = true;
            this.btnStopTimer.Click += new System.EventHandler(this.btnStopTimer_Click);
            // 
            // btnSecond
            // 
            this.btnSecond.AutoSize = true;
            this.btnSecond.Location = new System.Drawing.Point(413, 174);
            this.btnSecond.Name = "btnSecond";
            this.btnSecond.Size = new System.Drawing.Size(34, 20);
            this.btnSecond.TabIndex = 31;
            this.btnSecond.Text = "sec";
            // 
            // txtFreqSec
            // 
            this.txtFreqSec.Location = new System.Drawing.Point(264, 174);
            this.txtFreqSec.Name = "txtFreqSec";
            this.txtFreqSec.Size = new System.Drawing.Size(143, 26);
            this.txtFreqSec.TabIndex = 30;
            // 
            // lblFrequency
            // 
            this.lblFrequency.AutoSize = true;
            this.lblFrequency.Location = new System.Drawing.Point(174, 177);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(84, 20);
            this.lblFrequency.TabIndex = 29;
            this.lblFrequency.Text = "Frequency";
            // 
            // btnUploadDB
            // 
            this.btnUploadDB.Location = new System.Drawing.Point(257, 206);
            this.btnUploadDB.Name = "btnUploadDB";
            this.btnUploadDB.Size = new System.Drawing.Size(92, 34);
            this.btnUploadDB.TabIndex = 28;
            this.btnUploadDB.Text = "Upload";
            this.btnUploadDB.UseVisualStyleBackColor = true;
            this.btnUploadDB.Click += new System.EventHandler(this.btnUploadDB_Click);
            // 
            // gbProcedure
            // 
            this.gbProcedure.Controls.Add(this.label4);
            this.gbProcedure.Controls.Add(this.txtPHori2);
            this.gbProcedure.Controls.Add(this.txtPVert2);
            this.gbProcedure.Controls.Add(this.lblPHori2);
            this.gbProcedure.Controls.Add(this.lblPVert2);
            this.gbProcedure.Controls.Add(this.btnRmvProc);
            this.gbProcedure.Controls.Add(this.label1);
            this.gbProcedure.Controls.Add(this.txtProcedureName);
            this.gbProcedure.Controls.Add(this.lbProcedure);
            this.gbProcedure.Controls.Add(this.lblProcedure);
            this.gbProcedure.Controls.Add(this.txtProcedure);
            this.gbProcedure.Controls.Add(this.btnAddCells);
            this.gbProcedure.Controls.Add(this.txtPHori);
            this.gbProcedure.Controls.Add(this.txtPVert);
            this.gbProcedure.Controls.Add(this.btnAddProcess);
            this.gbProcedure.Controls.Add(this.lblPHori);
            this.gbProcedure.Controls.Add(this.btnProcedure);
            this.gbProcedure.Controls.Add(this.lblPVert);
            this.gbProcedure.Controls.Add(this.lbProcess);
            this.gbProcedure.Location = new System.Drawing.Point(1214, 49);
            this.gbProcedure.Name = "gbProcedure";
            this.gbProcedure.Size = new System.Drawing.Size(672, 372);
            this.gbProcedure.TabIndex = 1;
            this.gbProcedure.TabStop = false;
            this.gbProcedure.Text = "Procedure";
            // 
            // btnRmvProc
            // 
            this.btnRmvProc.Location = new System.Drawing.Point(557, 249);
            this.btnRmvProc.Name = "btnRmvProc";
            this.btnRmvProc.Size = new System.Drawing.Size(109, 55);
            this.btnRmvProc.TabIndex = 39;
            this.btnRmvProc.Text = "Remove Procedure";
            this.btnRmvProc.UseVisualStyleBackColor = true;
            this.btnRmvProc.Click += new System.EventHandler(this.btnRmvProc_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Procedure Name";
            // 
            // txtProcedureName
            // 
            this.txtProcedureName.Location = new System.Drawing.Point(10, 336);
            this.txtProcedureName.Name = "txtProcedureName";
            this.txtProcedureName.Size = new System.Drawing.Size(349, 26);
            this.txtProcedureName.TabIndex = 37;
            // 
            // lbProcedure
            // 
            this.lbProcedure.FormattingEnabled = true;
            this.lbProcedure.ItemHeight = 20;
            this.lbProcedure.Location = new System.Drawing.Point(537, 19);
            this.lbProcedure.Name = "lbProcedure";
            this.lbProcedure.ScrollAlwaysVisible = true;
            this.lbProcedure.Size = new System.Drawing.Size(129, 164);
            this.lbProcedure.TabIndex = 36;
            this.lbProcedure.SelectedIndexChanged += new System.EventHandler(this.lbProcedure_SelectedIndexChanged);
            // 
            // lblProcedure
            // 
            this.lblProcedure.AutoSize = true;
            this.lblProcedure.Location = new System.Drawing.Point(6, 216);
            this.lblProcedure.Name = "lblProcedure";
            this.lblProcedure.Size = new System.Drawing.Size(82, 20);
            this.lblProcedure.TabIndex = 35;
            this.lblProcedure.Text = "Procedure";
            // 
            // txtProcedure
            // 
            this.txtProcedure.Location = new System.Drawing.Point(10, 242);
            this.txtProcedure.Multiline = true;
            this.txtProcedure.Name = "txtProcedure";
            this.txtProcedure.Size = new System.Drawing.Size(349, 56);
            this.txtProcedure.TabIndex = 34;
            // 
            // btnAddCells
            // 
            this.btnAddCells.Location = new System.Drawing.Point(267, 72);
            this.btnAddCells.Name = "btnAddCells";
            this.btnAddCells.Size = new System.Drawing.Size(92, 34);
            this.btnAddCells.TabIndex = 33;
            this.btnAddCells.Text = "Add";
            this.btnAddCells.UseVisualStyleBackColor = true;
            this.btnAddCells.Click += new System.EventHandler(this.btnAddCells_Click);
            // 
            // txtPHori
            // 
            this.txtPHori.Location = new System.Drawing.Point(10, 45);
            this.txtPHori.Name = "txtPHori";
            this.txtPHori.Size = new System.Drawing.Size(94, 26);
            this.txtPHori.TabIndex = 21;
            // 
            // txtPVert
            // 
            this.txtPVert.Location = new System.Drawing.Point(110, 45);
            this.txtPVert.Name = "txtPVert";
            this.txtPVert.Size = new System.Drawing.Size(94, 26);
            this.txtPVert.TabIndex = 22;
            // 
            // btnAddProcess
            // 
            this.btnAddProcess.Location = new System.Drawing.Point(422, 189);
            this.btnAddProcess.Name = "btnAddProcess";
            this.btnAddProcess.Size = new System.Drawing.Size(109, 55);
            this.btnAddProcess.TabIndex = 26;
            this.btnAddProcess.Text = "Insert  Process";
            this.btnAddProcess.UseVisualStyleBackColor = true;
            this.btnAddProcess.Click += new System.EventHandler(this.btnAddProcess_Click);
            // 
            // lblPHori
            // 
            this.lblPHori.AutoSize = true;
            this.lblPHori.Location = new System.Drawing.Point(6, 22);
            this.lblPHori.Name = "lblPHori";
            this.lblPHori.Size = new System.Drawing.Size(81, 20);
            this.lblPHori.TabIndex = 23;
            this.lblPHori.Text = "Horizontal";
            // 
            // btnProcedure
            // 
            this.btnProcedure.Location = new System.Drawing.Point(557, 188);
            this.btnProcedure.Name = "btnProcedure";
            this.btnProcedure.Size = new System.Drawing.Size(109, 55);
            this.btnProcedure.TabIndex = 25;
            this.btnProcedure.Text = "Add Procedure";
            this.btnProcedure.UseVisualStyleBackColor = true;
            this.btnProcedure.Click += new System.EventHandler(this.btnProcedure_Click);
            // 
            // lblPVert
            // 
            this.lblPVert.AutoSize = true;
            this.lblPVert.Location = new System.Drawing.Point(106, 22);
            this.lblPVert.Name = "lblPVert";
            this.lblPVert.Size = new System.Drawing.Size(62, 20);
            this.lblPVert.TabIndex = 24;
            this.lblPVert.Text = "Vertical";
            // 
            // lbProcess
            // 
            this.lbProcess.FormattingEnabled = true;
            this.lbProcess.ItemHeight = 20;
            this.lbProcess.Location = new System.Drawing.Point(373, 19);
            this.lbProcess.Name = "lbProcess";
            this.lbProcess.ScrollAlwaysVisible = true;
            this.lbProcess.Size = new System.Drawing.Size(158, 164);
            this.lbProcess.TabIndex = 0;
            // 
            // btnRunProc
            // 
            this.btnRunProc.Location = new System.Drawing.Point(1481, 427);
            this.btnRunProc.Name = "btnRunProc";
            this.btnRunProc.Size = new System.Drawing.Size(109, 55);
            this.btnRunProc.TabIndex = 27;
            this.btnRunProc.Text = "Run Procedure";
            this.btnRunProc.UseVisualStyleBackColor = true;
            this.btnRunProc.Click += new System.EventHandler(this.btnRunProc_Click);
            // 
            // dgvOutput
            // 
            this.dgvOutput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOutput.Location = new System.Drawing.Point(1214, 485);
            this.dgvOutput.Name = "dgvOutput";
            this.dgvOutput.RowTemplate.Height = 28;
            this.dgvOutput.Size = new System.Drawing.Size(666, 215);
            this.dgvOutput.TabIndex = 28;
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(1210, 462);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(62, 20);
            this.lblOutput.TabIndex = 29;
            this.lblOutput.Text = "Output:";
            // 
            // txtPHori2
            // 
            this.txtPHori2.Location = new System.Drawing.Point(10, 127);
            this.txtPHori2.Name = "txtPHori2";
            this.txtPHori2.Size = new System.Drawing.Size(94, 26);
            this.txtPHori2.TabIndex = 40;
            // 
            // txtPVert2
            // 
            this.txtPVert2.Location = new System.Drawing.Point(110, 127);
            this.txtPVert2.Name = "txtPVert2";
            this.txtPVert2.Size = new System.Drawing.Size(94, 26);
            this.txtPVert2.TabIndex = 41;
            // 
            // lblPHori2
            // 
            this.lblPHori2.AutoSize = true;
            this.lblPHori2.Location = new System.Drawing.Point(6, 104);
            this.lblPHori2.Name = "lblPHori2";
            this.lblPHori2.Size = new System.Drawing.Size(81, 20);
            this.lblPHori2.TabIndex = 42;
            this.lblPHori2.Text = "Horizontal";
            // 
            // lblPVert2
            // 
            this.lblPVert2.AutoSize = true;
            this.lblPVert2.Location = new System.Drawing.Point(106, 104);
            this.lblPVert2.Name = "lblPVert2";
            this.lblPVert2.Size = new System.Drawing.Size(62, 20);
            this.lblPVert2.TabIndex = 43;
            this.lblPVert2.Text = "Vertical";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 20);
            this.label4.TabIndex = 44;
            this.label4.Text = "Optional*      To";
            // 
            // Monitor_Changed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 835);
            this.Controls.Add(this.gbProcedure);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.dgvOutput);
            this.Controls.Add(this.btnRunProc);
            this.Controls.Add(this.gbUploadDatabase);
            this.Controls.Add(this.gbTrack);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtSavePath);
            this.Controls.Add(this.lblLogData);
            this.Controls.Add(this.btnBGExcel);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.rtbMonitor);
            this.Name = "Monitor_Changed";
            this.Text = "Monitor_Changed";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Monitor_Changed_Load);
            this.gbTrack.ResumeLayout(false);
            this.gbTrack.PerformLayout();
            this.gbUploadDatabase.ResumeLayout(false);
            this.gbUploadDatabase.PerformLayout();
            this.gbProcedure.ResumeLayout(false);
            this.gbProcedure.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbMonitor;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnBGExcel;
        private System.Windows.Forms.Label lblLogData;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbTrack;
        private System.Windows.Forms.TextBox txtVert;
        private System.Windows.Forms.Label lblVertical;
        private System.Windows.Forms.Label lblHorizontal;
        private System.Windows.Forms.TextBox txtHori;
        private System.Windows.Forms.CheckBox chkTrackAll;
        private System.Windows.Forms.Button btnTrack;
        private System.Windows.Forms.ListBox lbTrack;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Label lblOptional;
        private System.Windows.Forms.Label lblVertical2;
        private System.Windows.Forms.Label lblHorizontal2;
        private System.Windows.Forms.TextBox txtHori2;
        private System.Windows.Forms.TextBox txtVert2;
        private System.Windows.Forms.GroupBox gbUploadDatabase;
        private System.Windows.Forms.Label btnSecond;
        private System.Windows.Forms.TextBox txtFreqSec;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Button btnUploadDB;
        private System.Windows.Forms.Button btnStopTimer;
        private System.Windows.Forms.Label lblUploadTime;
        private System.Windows.Forms.CheckBox chkChangedUpload;
        private System.Windows.Forms.ListBox lbProcess;
        private System.Windows.Forms.GroupBox gbProcedure;
        private System.Windows.Forms.TextBox txtProcedure;
        private System.Windows.Forms.Button btnAddCells;
        private System.Windows.Forms.TextBox txtPHori;
        private System.Windows.Forms.TextBox txtPVert;
        private System.Windows.Forms.Button btnAddProcess;
        private System.Windows.Forms.Label lblPHori;
        private System.Windows.Forms.Button btnProcedure;
        private System.Windows.Forms.Label lblPVert;
        private System.Windows.Forms.ListBox lbProcedure;
        private System.Windows.Forms.Label lblProcedure;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProcedureName;
        private System.Windows.Forms.Button btnRunProc;
        private System.Windows.Forms.Button btnRmvProc;
        private System.Windows.Forms.DataGridView dgvOutput;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPHori2;
        private System.Windows.Forms.TextBox txtPVert2;
        private System.Windows.Forms.Label lblPHori2;
        private System.Windows.Forms.Label lblPVert2;
    }
}