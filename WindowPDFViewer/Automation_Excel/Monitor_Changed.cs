﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;
using System.Data.Common;

namespace WindowPDFViewer.Automation_Excel
{
    public partial class Monitor_Changed : Form
    {
        Thread tread1 = new Thread(new ThreadStart(saveLogFile));
        //  Thread treadInsert = new Thread(new ThreadStart(insertExcel));
        //Thread tread2UploadDB = new Thread(new ThreadStart(uploadDatabaseInitiator));
        private System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker(); 
        private System.Threading.Timer TimerUpload;
        public static List<LogData> LD = new List<LogData>();
        //public static List<LogData> tempLD = new List<LogData>();
        private List<CellsTrack> ListTrack = new List<CellsTrack>();
        private List<Procedure> ListProcedure = new List<Procedure>();

        private static string excelPath;
        private static string savePath;
        private int count = 1;
        private int timerCount = 0;
        private bool firstLoad = true;
        private bool fileOpen = false;

        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        SqlConnection connection;
        SqlCommand command;
        String sql;

        public Monitor_Changed()
        {
            InitializeComponent();  
            Closing += OnWindowClosing;
        }
        private void Monitor_Changed_Load(object sender, EventArgs e)
        {
            string ConnectionString1 = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\manhi\Documents\LogDatabase.mdf;Integrated Security=True";
            connection = new SqlConnection(ConnectionString1);
            connection.Open();
            sql = "insert into LogData values (@timeStamp , @targetSheet , @cellsRange , @action, @value ) ";
            command = new SqlCommand(sql, connection);

            lbProcess.Items.Add("sum");
            lbProcess.Items.Add("sum[]");
            lbProcess.Items.Add("minus");
            lbProcess.Items.Add("minus[]");
            lbProcess.Items.Add("multiply");
            lbProcess.Items.Add("multiply[]");
            lbProcess.Items.Add("divide");
            lbProcess.Items.Add("divide[]");
            lbProcess.Items.Add("average");
            lbProcess.Items.Add("insert");
            lbProcess.Items.Add("delete");
            lbProcess.Items.Add("merge");


            backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker1_DoWork);
        }
        

        private Excel.AppEvents_WorkbookOpenEventHandler EventDel_WorkbookOpen;
        private Excel.WorkbookEvents_SheetChangeEventHandler EventDel_WBSheetChange;

        private Excel.AppEvents_SheetChangeEventHandler EventDel_AppSheetChange;
        private Excel.WorkbookEvents_AfterSaveEventHandler EventDel_WorkbookSave;
        private Excel.WorkbookEvents_BeforeCloseEventHandler EventDel_BeforeClose;
        private Excel.WorkbookEvents_NewChartEventHandler EventDel_NewChart;

        public Excel.Application xlAppNew = null;
        private Excel.Workbook xlBook;
        private Excel.Worksheet xlSheet;

        //insert cell
        private static Excel.Workbook xlBookStatic;
        //private static double combineValueStatic;
        //private static string insertRangeStatic;
        private static int procIndex = 0;

        private void AddHandler(string strFPath, bool backGround, bool TrackAllCell)
        {
            string time = DateTime.Now.ToString("dd/MM/yy h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string Action = "Starts Monitoring";
            firstLoad = false;
            timerCount = 0;

            fileOpen = true;
            if (backGround == true)
                try
                {
                    xlAppNew = Marshal.GetActiveObject("Excel.Application") as Excel.Application;
                    xlBook = xlAppNew.ActiveWorkbook;
                }
                catch (Exception) { }
            else if (backGround == false)
            {
                xlAppNew = new Excel.Application();
                xlAppNew.Visible = true;
                xlAppNew.UserControl = true;
                xlBook = xlAppNew.Workbooks.Open(strFPath, 0, false, 5,
            "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            }
            if (xlAppNew.ActiveWorkbook != null)
            {
                rtbMonitor.Text += "No." + count + "   " + time + "   Action: " + Action + ":  " + xlAppNew.ActiveWorkbook.Name + "\n";
                count++;
                LD.Add(new LogData()
                {
                    dateTime = dt,
                    action = Action
                });

                //while excel open event handler
                //EventDel_WorkbookOpen = new Excel.AppEvents_WorkbookOpenEventHandler(WBopen);
                //xlAppNew.WorkbookOpen += EventDel_WorkbookOpen;

                //while cells value changed event handler
                if (TrackAllCell == true)
                {
                    EventDel_AppSheetChange = new Excel.AppEvents_SheetChangeEventHandler(AppSheetChange);
                }
                else if (TrackAllCell == false)
                {
                    EventDel_AppSheetChange = new Excel.AppEvents_SheetChangeEventHandler(CustAppSheetChange);
                }
                xlAppNew.SheetChange += EventDel_AppSheetChange;

                //while excel save event handler
                EventDel_WorkbookSave = new Excel.WorkbookEvents_AfterSaveEventHandler(ThisWorkbook_AfterSave);
                xlBook.AfterSave += EventDel_WorkbookSave;

                //before excel close event trigger
                EventDel_BeforeClose = new Excel.WorkbookEvents_BeforeCloseEventHandler(ThisWorkbook_BeforeClose);
                xlBook.BeforeClose += EventDel_BeforeClose;

                //while create new chart
                EventDel_NewChart = new Excel.WorkbookEvents_NewChartEventHandler(WorkbookEvents_NewChartEventHandler);
                xlBook.NewChart += EventDel_NewChart;
            }
            
        }// end AddHandler

        //workbook create new chart
        private void WorkbookEvents_NewChartEventHandler(Chart Ch)
        {
            string time = DateTime.Now.ToString("dd/MM/yy  h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string Action = "Add Chart";

            if (rtbMonitor.InvokeRequired)
            {
                rtbMonitor.Invoke(new MethodInvoker(delegate { rtbMonitor.Text += "No." + count + "   " + time + "   Action: " + Action + "\n"; }));
            }
            count++;
            LD.Add(new LogData()
            {
                dateTime = dt,
                action = Action
            });

        }

        //workbook before close
        private void ThisWorkbook_BeforeClose(ref bool Cancel)
        {
            string time = DateTime.Now.ToString("dd/MM/yy  h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string Action = "Workbook Close";
            if (!xlAppNew.ActiveWorkbook.Saved)
            {
                DialogResult result = MessageBox.Show("Do you want to save the " +
                    "changes you made to " + xlAppNew.ActiveWorkbook.Name + "?", "Example",
                    MessageBoxButtons.YesNoCancel);

                switch (result)
                {
                    case DialogResult.Yes:
                        xlAppNew.ActiveWorkbook.Save();
                        if (rtbMonitor.InvokeRequired)
                        {
                            rtbMonitor.Invoke(new MethodInvoker(delegate {
                                rtbMonitor.Text += "No." + count +
                        "   " + time + "   Action: " + Action + "\n";
                            }));
                        }
                        count++;
                        LD.Add(new LogData()
                        {
                            dateTime = dt,
                            action = Action
                        });
                        if (chkChangedUpload.Checked == true)
                        {
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@timeStamp", dt);
                            command.Parameters.AddWithValue("@targetSheet", DBNull.Value);
                            command.Parameters.AddWithValue("@cellsRange", DBNull.Value);
                            command.Parameters.AddWithValue("@action", Action);

                            using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                Reader.Close();
                            }
                            if (lblUploadTime.InvokeRequired)
                            {
                                lblUploadTime.Invoke(new MethodInvoker(delegate {
                                    lblUploadTime.Text = "Upload Log Time: " + dt
                                     + "\nAction: " + Action;
                                }));
                            }
                        }
                        fileOpen = false;
                        break;

                    case DialogResult.Cancel:
                        Cancel = true;
                        break;

                    // The following code ensures that the default Save File 
                    // dialog is not displayed.
                    case DialogResult.No:
                        xlAppNew.ActiveWorkbook.Saved = true;
                        if (rtbMonitor.InvokeRequired)
                        {
                            rtbMonitor.Invoke(new MethodInvoker(delegate {
                                rtbMonitor.Text += "No." + count +
                            "   " + time + "   Action: " + Action + "\n";
                            }));
                        }
                        count++;
                        LD.Add(new LogData()
                        {
                            dateTime = dt,
                            action = Action
                        });
                        if (chkChangedUpload.Checked == true)
                        {
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@timeStamp", dt);
                            command.Parameters.AddWithValue("@targetSheet", DBNull.Value);
                            command.Parameters.AddWithValue("@cellsRange", DBNull.Value);
                            command.Parameters.AddWithValue("@action", Action);

                            using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                Reader.Close();
                            }
                            if (lblUploadTime.InvokeRequired)
                            {
                                lblUploadTime.Invoke(new MethodInvoker(delegate {
                                    lblUploadTime.Text = "Upload Log Time: " + dt
                                     + "\nAction: " + Action;
                                }));
                            }
                        }
                        fileOpen = false;
                        break;
                }
            }
            else
            {
                if (rtbMonitor.InvokeRequired)
                {
                    rtbMonitor.Invoke(new MethodInvoker(delegate {
                        rtbMonitor.Text += "No." + count +
                    "   " + time + "   Action: " + Action + "\n";
                    }));
                }
                count++;
                LD.Add(new LogData()
                {
                    dateTime = dt,
                    action = Action
                });
                fileOpen = false;
            }
        }

        //workbook before save
        private void ThisWorkbook_AfterSave(bool SaveAsUI)
        {
            string time = DateTime.Now.ToString("dd/MM/yy h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string Action = "File Save";

            if (rtbMonitor.InvokeRequired)
            {
                rtbMonitor.Invoke(new MethodInvoker(delegate {
                    rtbMonitor.Text += "No." + count +
                "   " + time + "   Action: " + Action + "\n";
                }));
            }
            count++;
            LD.Add(new LogData()
            {
                dateTime = dt,
                action = Action
            });
            if (chkChangedUpload.Checked == true)
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@timeStamp", dt);
                command.Parameters.AddWithValue("@targetSheet",  DBNull.Value);
                command.Parameters.AddWithValue("@cellsRange", DBNull.Value);
                command.Parameters.AddWithValue("@action", Action);
                command.Parameters.AddWithValue("@value", DBNull.Value);

                using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    Reader.Close();
                }
                if (lblUploadTime.InvokeRequired)
                {
                    lblUploadTime.Invoke(new MethodInvoker(delegate {
                        lblUploadTime.Text = "Upload Log Time: " + dt
                         + "\nAction: " + Action;
                    }));
                }
            }
        }

        //open excel trigger event * no use yet
        private void WBopen(Excel.Workbook Wb)
        {
            //EventDel_WBSheetChange = new Excel.WorkbookEvents_SheetChangeEventHandler(WBSheetChange);
            //xlAppNew.ActiveWorkbook.SheetChange += EventDel_WBSheetChange;
            MessageBox.Show("Found Excel");
        }// end WBopen

        private void WBSheetChange(object Sh, Excel.Range Target)
        {
            MessageBox.Show(Target.Worksheet.Name.ToString() + "WB change");
        }
        
        //trigger this event while any cell value changed
        private void AppSheetChange(object Sh, Excel.Range Target)
        {

            string changedRange = Target.get_Address(Excel.XlReferenceStyle.xlA1);
            string time = DateTime.Now.ToString("dd/MM/yy h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string ts = Target.Worksheet.Name.ToString();
            string cr = changedRange.Replace("$", "");
            string Action = "Value Changed";

            xlSheet = xlBook.Sheets[Target.Worksheet.Index];
            Excel.Range range = xlSheet.get_Range(Target.get_Address(Excel.XlReferenceStyle.xlA1));
            string changedValue;
            var changedAnyValue = range.Cells.Value;
            string CombineValue="";

            if (changedAnyValue != null)
            {
                Type valueType = changedAnyValue.GetType();
                if (valueType.IsArray)
                {
                    foreach (var items in changedAnyValue)
                    {
                        if (items != null)
                            CombineValue += items.ToString() + ",";
                        else
                            CombineValue += "[],";
                    }
                    changedValue = CombineValue.ToString().Substring(0, CombineValue.Length-1);
                }
                else
                {
                    if (double.TryParse(range.Cells.Value2.ToString(), out double d))
                    {
                        //double doubleChangeValue = range.Cells.Value2;
                        //changedValue = doubleChangeValue.ToString();
                        changedValue = ((double)range.Cells.Value2).ToString();
                    }
                    else
                    {
                        changedValue = range.Cells.Value;
                    }
                }
            }
            else
            {
                changedValue = "[]";
            }
           
            

            if (rtbMonitor.InvokeRequired)
            {
                rtbMonitor.Invoke(new MethodInvoker(delegate {
                    rtbMonitor.Text += "No." + count + "   " + time + "  Target sheet: "
                + ts + "   Range: " + cr +  "   Action: " + Action + "    Cell(s) Value: "+changedValue+"\n";
                }));
            }

            LD.Add(new LogData()
            {
                dateTime = dt,
                targetSheet = Target.Worksheet.Name.ToString(),
                changedRange = changedRange.Replace("$", ""),
                action = Action,
                value = changedValue
            });
            if (chkChangedUpload.Checked == true)
            {
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@timeStamp", dt);
                command.Parameters.AddWithValue("@targetSheet", ((object)ts) ?? DBNull.Value);
                command.Parameters.AddWithValue("@cellsRange", ((object)cr) ?? DBNull.Value);
                command.Parameters.AddWithValue("@action", Action);
                command.Parameters.AddWithValue("@value", changedValue);

                using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    Reader.Close();
                }
                if (lblUploadTime.InvokeRequired)
                {
                    lblUploadTime.Invoke(new MethodInvoker(delegate {
                        lblUploadTime.Text = "Upload Log Time: " + dt
                        + "\nSheet: " + ts + "\nRange: " + cr + "\nAction: " + Action
                        +"\nValue: "+changedValue;
                    }));
                }
            }

            count++;
        }

        //trigger this event while custome cell value changed
        private void CustAppSheetChange(object Sh, Excel.Range Target)
        {
            string changedRange = Target.get_Address(Excel.XlReferenceStyle.xlA1);
            string time = DateTime.Now.ToString("dd/MM/yy h:mm:ss tt");
            DateTime dt = DateTime.Now;
            string ts = Target.Worksheet.Name.ToString();
            string cr = changedRange.Replace("$", "");
            string Action = "Value Changed";
            string[] arrayRange = changedRange.Split('$');
            string HoriIndex = arrayRange[1];
            string VertIndex = arrayRange[2];
            string customeRange;

            for (int Index = 0; Index < ListTrack.Count; Index++)
            {
                //get changed cell value
                int page = int.Parse(Target.Worksheet.Index.ToString());
                if (ListTrack[Index].HorizotalIndex2 == null && ListTrack[Index].VerticalIndex2 == null)
                {
                    customeRange = ListTrack[Index].HorizotalIndex + ListTrack[Index].VerticalIndex;//user set range
                }
                else
                {
                    customeRange = ListTrack[Index].HorizotalIndex + ListTrack[Index].VerticalIndex + ":" + ListTrack[Index].HorizotalIndex2 + ListTrack[Index].VerticalIndex2;
                }
                

                xlSheet = (Excel.Worksheet)xlBook.Worksheets.get_Item(page);
                Excel.Range xlMultiRange = xlSheet.get_Range(changedRange, Type.Missing);
                Excel.Range xlCustomeRange = xlSheet.get_Range(customeRange, Type.Missing);

                //get value
                Excel.Range range = xlSheet.get_Range(Target.get_Address(Excel.XlReferenceStyle.xlA1));
                string changedValue;
                var changedAnyValue = range.Cells.Value;
                string CombineValue = "";
                Type valueType = changedAnyValue.GetType();
                

                if (xlAppNew.Intersect(xlCustomeRange, xlMultiRange) != null)
                {
                    //get changed value
                    if (valueType.IsArray)
                    {
                        foreach (var items in changedAnyValue)
                        {
                            if (items != null)
                                CombineValue += items.ToString() + " ";
                            else
                                CombineValue += "[] ";
                        }
                        changedValue = CombineValue.ToString();
                    }
                    else
                    {
                        if (double.TryParse(range.Cells.Value2.ToString(), out double d))
                        {
                            changedValue = ((double)range.Cells.Value2).ToString();
                        }
                        else
                        {
                            changedValue = range.Cells.Value;
                        }
                    }

                    if (rtbMonitor.InvokeRequired)
                    {
                        rtbMonitor.Invoke(new MethodInvoker(delegate {
                            rtbMonitor.Text += "No." + count + "   " + time + "  Target sheet: "
                            + Target.Worksheet.Name.ToString() + "   Range: " + customeRange + "   Action: " + Action
                           + "    Cell(s) Value: " + changedValue + "\n";
                        }));
                    }

                    LD.Add(new LogData()
                    {
                        dateTime = dt,
                        targetSheet = Target.Worksheet.Name.ToString(),
                        changedRange = customeRange,
                        action = Action,
                        value = changedValue
                    });
                    if (chkChangedUpload.Checked == true)
                    {
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@timeStamp", dt);
                        command.Parameters.AddWithValue("@targetSheet", ((object)ts) ?? DBNull.Value);
                        command.Parameters.AddWithValue("@cellsRange", ((object)cr) ?? DBNull.Value);
                        command.Parameters.AddWithValue("@action", Action);
                        command.Parameters.AddWithValue("@value", changedValue);

                        using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            Reader.Close();
                        }
                        if (lblUploadTime.InvokeRequired)
                        {
                            lblUploadTime.Invoke(new MethodInvoker(delegate {
                                lblUploadTime.Text = "Upload Log Time: " + dt
                                + "\nSheet: " + ts + "\nRange: " + cr + "\nAction: " + Action 
                                + "\nValue: " + changedValue;
                            }));
                        }
                    }
                    count++;
                }
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.DefaultExt = ".xlsx";
            openFileDialog1.Filter = "(.xlsx)|*.xlsx";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //open click will release all marshal, different from background
                releaseExcel();
                bool TrackAllCell = false;
                txtPath.Text = openFileDialog1.FileName.ToString();
                excelPath = openFileDialog1.FileName.ToString();

                if (chkTrackAll.Checked == true)
                {
                    TrackAllCell = true;
                    AddHandler(excelPath, false, TrackAllCell);
                }
                else
                {
                    TrackAllCell = false;
                    AddHandler(excelPath, false, TrackAllCell);
                }
            }
        }

        private void releaseExcel()
        {

            rtbMonitor.Invoke(new MethodInvoker(delegate
            {
                LD.Clear();
                count = 1;
                rtbMonitor.Clear();
            }));
            xlAppNew = null;
            xlBook = null;
            if (xlAppNew != null)
            {
                if (xlBook != null && fileOpen == true)
                {
                    try
                    {
                        xlBook.Close();
                        Marshal.ReleaseComObject(xlBook);
                        xlBook = null;
                    }
                    catch (Exception) { }
                    fileOpen = false;
                }
                try
                {
                    xlAppNew.Quit();
                    Marshal.ReleaseComObject(xlAppNew);
                    xlAppNew = null;
                }
                catch (Exception) { }
            }

        }

        /// <summary>
        /// Close excel background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            releaseExcel();
            connection.Close();
        }

        //check event exists
        public bool IsEventHandlerRegistered(Delegate prospectiveHandler)
        {
            try
            {
                prospectiveHandler.GetInvocationList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void btnBGExcel_Click(object sender, EventArgs e)
        {
            bool TrackAllCell = false;

            LD.Clear();
            count = 1;
            rtbMonitor.Clear();

            if (IsEventHandlerRegistered(EventDel_AppSheetChange) && IsEventHandlerRegistered(EventDel_WorkbookSave)
                && IsEventHandlerRegistered(EventDel_BeforeClose) && IsEventHandlerRegistered(EventDel_NewChart))
            {
                xlAppNew.SheetChange -= EventDel_AppSheetChange;
                xlBook.AfterSave -= EventDel_WorkbookSave;
                xlBook.BeforeClose -= EventDel_BeforeClose;
                xlBook.NewChart -= EventDel_NewChart;
            }
       
            if (chkTrackAll.Checked == true)
            {
                TrackAllCell = true;
                AddHandler(excelPath, true, TrackAllCell);
            }
            else
            {
                TrackAllCell = false;
                AddHandler(excelPath, true, TrackAllCell);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.DefaultExt = ".xlsx";
            saveFileDialog1.Filter = "(.xlsx)|*.xlsx";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                savePath = saveFileDialog1.FileName.ToString();
                txtSavePath.Text = saveFileDialog1.FileName.ToString();
            }
            tread1.Start();
        }

        private static void saveLogFile()
        {
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Add();
            Excel._Worksheet excelSheet = excelBook.Sheets[1];
            excelApp.DisplayAlerts = false;

            excelSheet.Cells[1][1] = "dateTime";
            excelSheet.Cells[2][1] = "targetSheet";
            excelSheet.Cells[3][1] = "changedRange";
            excelSheet.Cells[4][1] = "action";

            for (int IndexRow = 0; IndexRow < LD.Count; IndexRow++)
            {
                excelSheet.Cells[1][IndexRow + 2] = LD[IndexRow].dateTime;
                excelSheet.Cells[2][IndexRow + 2] = LD[IndexRow].targetSheet;
                excelSheet.Cells[3][IndexRow + 2] = LD[IndexRow].changedRange;
                excelSheet.Cells[4][IndexRow + 2] = LD[IndexRow].action;
            }
            if (excelApp.ActiveWorkbook != null)
            {
                excelApp.ActiveWorkbook.SaveAs(savePath);
            }

            if (excelApp != null)
            {
                if (excelBook != null)
                {
                    excelBook.Close();
                    Marshal.ReleaseComObject(excelBook);
                }
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
            }
        }

        private void chkTrackAll_CheckedChanged(object sender, EventArgs e)
        {
            if (firstLoad != true)
            {
                xlAppNew.SheetChange -= EventDel_AppSheetChange;

                if (chkTrackAll.Checked == true)
                {
                    EventDel_AppSheetChange = new Excel.AppEvents_SheetChangeEventHandler(AppSheetChange);
                }
                else if (chkTrackAll.Checked == false)
                {
                    EventDel_AppSheetChange = new Excel.AppEvents_SheetChangeEventHandler(CustAppSheetChange);
                }
                xlAppNew.SheetChange += EventDel_AppSheetChange;
            }
        }

        private void btnTrack_Click(object sender, EventArgs e)
        {
            string Hori1 = txtHori.Text;
            string Vert1 = txtVert.Text;
            string Hori2 = txtHori2.Text;
            string Vert2 = txtVert2.Text;
            var isDecimal = decimal.TryParse(Vert1, out decimal d);
            var isNumeric = double.TryParse(Hori1, out double c);
            var isDecimal2 = decimal.TryParse(Vert2, out decimal a);
            var isNumeric2 = double.TryParse(Hori2, out double b);

            if (!string.IsNullOrEmpty(Hori1) && !string.IsNullOrEmpty(Vert1) && !isNumeric && isDecimal)
            {
                Hori1 = Hori1.ToUpper();

                if (!string.IsNullOrEmpty(Hori2) && !string.IsNullOrEmpty(Vert2) && !isNumeric2 && isDecimal2)
                {
                    Hori2 = Hori2.ToUpper();
                    ListTrack.Add(new CellsTrack() { HorizotalIndex = Hori1, VerticalIndex = Vert1, HorizotalIndex2 = Hori2, VerticalIndex2 = Vert2 });
                    lbTrack.BeginUpdate();
                    lbTrack.Items.Add(Hori1 + Vert1 + ":" + Hori2 + Vert2);
                    lbTrack.EndUpdate();
                }
                else
                {
                    ListTrack.Add(new CellsTrack() { HorizotalIndex = Hori1, VerticalIndex = Vert1 });
                    lbTrack.BeginUpdate();
                    lbTrack.Items.Add(Hori1 + Vert1);
                    lbTrack.EndUpdate();
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbTrack.Items.Count > 0)
            {
                ListTrack.RemoveAt(lbTrack.SelectedIndex);
                lbTrack.Items.RemoveAt(lbTrack.SelectedIndex);
            }
        }
       
        private void btnUploadDB_Click(object sender, EventArgs e)
        {
           string inputValue = txtFreqSec.Text;
            var isNumeric = double.TryParse(inputValue, out double c);

            if (isNumeric == true)
            {
                int frequencySecond = int.Parse(inputValue) * 1000;
                TimerUpload = new System.Threading.Timer(new TimerCallback(uploadDB), null, 1000, frequencySecond);
            }    
        }

        private void uploadDB(object state)
        {
            for (int index = timerCount; index < LD.Count; index++)
            {
                DateTime dt = LD[index].dateTime;
                string ts = LD[index].targetSheet;
                string cr = LD[index].changedRange;
                string ac = LD[index].action;
                string cv = LD[index].value;

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@timeStamp", dt);
                command.Parameters.AddWithValue("@targetSheet", ((object)ts) ?? DBNull.Value);
                command.Parameters.AddWithValue("@cellsRange", ((object)cr) ?? DBNull.Value);
                command.Parameters.AddWithValue("@action", ac);
                command.Parameters.AddWithValue("@value", ((object)cv) ?? DBNull.Value);

                using (DbDataReader Reader = command.ExecuteReader(CommandBehavior.SingleRow))
                {
                    Reader.Close();
                }
                //System.Windows.Forms.TextBox tb1 = new System.Windows.Forms.TextBox();
                //tb1.Location = new System.Drawing.Point(6, 22);
                //tb1.Size = new System.Drawing.Size(133, 20);
                //tb1.Text = "Upload Log Time: " + dt
                //        + "\nSheet: " + ts + "\nRange: " + cr + "\nAction: " + ac;
                //tb1.Show();
                if (lblUploadTime.InvokeRequired)
                {
                    lblUploadTime.Invoke(new MethodInvoker(delegate
                    {
                        lblUploadTime.Text = "Upload Log Time: " + dt
                        + "\nSheet: " + ts + "\nRange: " + cr + "\nAction: " + ac;
                    }));
                }
                timerCount++;
            }    
        }

        private void btnStopTimer_Click(object sender, EventArgs e)
        {
            TimerUpload.Change(Timeout.Infinite,Timeout.Infinite);
        }

        private void btnAddCells_Click(object sender, EventArgs e)
        {
            string Hori1 = txtPHori.Text;
            string Vert1 = txtPVert.Text;
            string Hori2 = txtPHori2.Text;
            string Vert2 = txtPVert2.Text;

            string Procedure = txtProcedure.Text;

            var isNumeric = double.TryParse(Hori1, out double c);
            var isDecimal = decimal.TryParse(Vert1, out decimal d);
            var isNumeric2 = double.TryParse(Hori2, out double a);
            var isDecimal2 = decimal.TryParse(Vert2, out decimal b);

            //if (!string.IsNullOrEmpty(Hori1) && !isNumeric && string.IsNullOrEmpty(Vert1))
            //{
            //    Hori1 = Hori1.ToUpper();
            //    Procedure += "(cells:"+ Hori1 + ")";
            //}
            //else if (!string.IsNullOrEmpty(Vert1) && isDecimal && string.IsNullOrEmpty(Hori1))
            //{
            //    Procedure += "(cells:" + Vert1 + ")";
            //}
            //else
            if (!string.IsNullOrEmpty(Hori1) && !string.IsNullOrEmpty(Vert1) && !isNumeric && isDecimal 
                && !string.IsNullOrEmpty(Hori2) && !string.IsNullOrEmpty(Vert2) && !isNumeric2 && isDecimal2)
            {
                Hori1 = Hori1.ToUpper();
                Hori2 = Hori2.ToUpper();

                Procedure += "(cells:" + Hori1 + "," + Vert1 + ":" + Hori2 + "," + Vert2 + ")";
            }
            else if (!string.IsNullOrEmpty(Hori1) && !string.IsNullOrEmpty(Vert1) && !isNumeric && isDecimal)
            {
                Hori1 = Hori1.ToUpper();
                Procedure += "(cells:" + Hori1 + "," + Vert1 + ")";
            }
            txtProcedure.Text = Procedure;
        }

        private void btnAddProcess_Click(object sender, EventArgs e)
        {
            string Procedure = txtProcedure.Text;
          
            switch (lbProcess.SelectedItem)
            {
                case "sum"://add
                    Procedure += "(p:sum)";
                    break;
                case "sum[]":
                    Procedure += "(p:sum[])";
                    break;
                case "minus":
                    Procedure += "(p:minus)";
                    break;
                case "minus[]":
                    Procedure += "(p:minus[])";
                    break;
                case "multiply":
                    Procedure += "(p:multiply)";
                    break;
                case "multiply[]":
                    Procedure += "(p:multiply[])";
                    break;
                case "divide":
                    Procedure += "(p:divide)";
                    break;
                case "divide[]":
                    Procedure += "(p:divide[])";
                    break;
                case "average":
                    Procedure += "(p:average)";
                    break;   
                case "insert":
                    Procedure += "(p:insert)";
                    break;
                case "delete":
                    Procedure += "(p:delete)";
                    break;
                case "merge":
                    Procedure += "(p:merge)";
                    break;

            }
            txtProcedure.Text = Procedure;
        }
        
        private void btnProcedure_Click(object sender, EventArgs e)
        {
            string ProcedureName = txtProcedureName.Text.ToString();
            string ProcedureProcess = txtProcedure.Text;
            ListProcedure.Add(new Procedure() {ProcName = ProcedureName , ProcProcess = ProcedureProcess });

            lbProcedure.BeginUpdate();
            lbProcedure.Items.Add(ProcedureName);
            lbProcedure.EndUpdate();

            txtProcedure.Clear();
            txtProcedureName.Clear();
        }

        private void btnRunProc_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(2000);
  
        }


        private void btnRmvProc_Click(object sender, EventArgs e)
        {
            //later do control to remove from db
            if (lbProcedure.Items.Count > 0)
            {
                ListProcedure.RemoveAt(lbProcedure.SelectedIndex);
                lbProcedure.Items.RemoveAt(lbProcedure.SelectedIndex);
            }
        }

        private void lbProcedure_SelectedIndexChanged(object sender, EventArgs e)
        {
            procIndex = lbProcedure.SelectedIndex;
            string process;
            if (procIndex < ListProcedure.Count && procIndex>=0)
            {
                process = ListProcedure[procIndex].ProcProcess;
                string name = ListProcedure[procIndex].ProcName;
                txtProcedure.Text = process;
                txtProcedureName.Text = name;
            }        
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker helperBW = sender as BackgroundWorker;
            int arg = (int)e.Argument;
            e.Result = BackgroundProcessLogicMethod(helperBW, arg);
            if (helperBW.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private int BackgroundProcessLogicMethod(BackgroundWorker bw, int a)
        {
            int result = 0;

            string emptyString = "";
            string process = ListProcedure[procIndex].ProcProcess.Replace("(", emptyString);
            string[] processArray = process.Split(')');
            List<string> ListCells = new List<string>();
            List<string> ListProcess = new List<string>();

            for (int index = 0; index < processArray.Length; index++)
            {
                if (processArray[index].Contains("cells:"))
                {
                    ListCells.Add((processArray[index].Substring(6)).Replace(",", emptyString));
                }
            }

            for (int index = 0; index < processArray.Length; index++)
            {
                if (processArray[index].Contains("p:"))
                {
                    ListProcess.Add(processArray[index].Substring(2));
                }
            }

            Excel._Worksheet xlSheet = xlBook.ActiveSheet;

            int count = 0;
            double combineValue;
            string calculationIndex;
            //get first cells value
            Excel.Range range1 = xlSheet.get_Range(ListCells[count]);

            var firstValue = range1.Cells.Value;
            if (double.TryParse(firstValue.ToString(), out double d)) //check is double
            {
                combineValue = firstValue;
                Excel.Range range2 = null;

                foreach (var items in ListProcess)
                {
                    if ((count + 1) < ListCells.Count)
                    {
                        range2 = xlSheet.get_Range(ListCells[count + 1]);
                    }

                    var secondValue = range2.Cells.Value;

                    if (items == "sum")
                    {
                        if(secondValue != null)
                        {
                            if(double.TryParse(secondValue.ToString(), out double e))
                            {
                                if ((count + 1) < ListCells.Count)
                                    combineValue += secondValue;
                            }    
                        }    
                        count++;
                    }
                    else if (items.Contains("sum["))
                    {
                        calculationIndex = items.Substring(4);
                        combineValue += double.Parse(calculationIndex.Substring(0, calculationIndex.Length - 1));
                    }
                    else if (items == "minus")
                    {
                        if (secondValue != null)
                        {
                            if (double.TryParse(secondValue.ToString(), out double f))
                            {
                                if ((count + 1) < ListCells.Count)
                                    combineValue -= secondValue;
                            }
                        }
                        count++;

                    }
                    else if (items.Contains("minus["))
                    {
                        calculationIndex = items.Substring(6);
                        combineValue -= double.Parse(calculationIndex.Substring(0, calculationIndex.Length - 1));
                    }
                    else if (items == "multiply")
                    {
                        if (secondValue != null)
                        {
                            if (double.TryParse(secondValue.ToString(), out double g))
                            {
                                if ((count + 1) < ListCells.Count)
                                    combineValue *= secondValue;
                            }
                        }
                        count++;

                    }
                    else if (items.Contains("multiply["))
                    {
                        calculationIndex = items.Substring(9);
                        combineValue *= double.Parse(calculationIndex.Substring(0, calculationIndex.Length - 1));
                    }
                    else if (items == "divide")
                    {
                        if (secondValue != null)
                        {
                            if (double.TryParse(secondValue.ToString(), out double h))
                            {
                                if ((count + 1) < ListCells.Count)
                                    combineValue = Math.Round((combineValue / secondValue), 3);
                            }
                        }
                        count++;

                    }
                    else if (items.Contains("divide["))
                    {
                        calculationIndex = items.Substring(7);
                        combineValue = Math.Round((combineValue / double.Parse(calculationIndex.Substring(0, calculationIndex.Length - 1))), 3);
                        count++;

                    }
                    else if (items == "average")
                    {
                        combineValue = Math.Round((combineValue / (count + 1)), 3);

                    }
                    else if (items == "insert")
                    {
                        if ((count + 1) < ListCells.Count)
                        {
                            xlSheet.get_Range(ListCells[count + 1]).Value2 = combineValue;
                            count++;
                        }
                    }
                    else if (items == "delete")
                    {                   
                        xlSheet.get_Range(ListCells[count]).Value2 = "";                 
                    }
                    else if (items == "merge")
                    {
                        if ((count + 1) < ListCells.Count)
                        {
                            xlAppNew.DisplayAlerts = false;
                            xlSheet.Range[ListCells[count], ListCells[count + 1]].Merge();
                            xlAppNew.DisplayAlerts = true;
                            count++;
                        }
                    }
                }
                //string[] test = new string[1];
                //test[0] = combineValue.ToString();
                // dgvOutput.DataSource = st;//for debug check 
            }
            else
            {
                foreach (var items in ListProcess)
                {
                    if (items == "insert")
                    {
                        if ((count + 1) < ListCells.Count)
                        {
                            xlSheet.get_Range(ListCells[count + 1]).Value2 = firstValue;
                            count++;
                        }
                    }
                    else if (items == "delete")
                    {
                        xlSheet.get_Range(ListCells[count]).Value2 = "";
                    }
                    else if (items == "merge")
                    {
                        if ((count + 1) < ListCells.Count)
                        {
                            xlAppNew.DisplayAlerts = false;
                            xlSheet.Range[ListCells[count], ListCells[count + 1]].Merge();
                            xlAppNew.DisplayAlerts = true;
                            count++;
                        }
                    }
                }
            }

            return result;
        }
    }
}
