﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowPDFViewer.Automation_Excel
{
    public class Procedure
    {
        public string ProcName { get; set; }
        public string ProcProcess { get; set; }
    }
}
