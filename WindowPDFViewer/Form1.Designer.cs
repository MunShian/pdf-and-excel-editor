﻿namespace WindowPDFViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnFont = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.gbExport = new System.Windows.Forms.GroupBox();
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnText = new System.Windows.Forms.Button();
            this.btnWord = new System.Windows.Forms.Button();
            this.rtbContent = new System.Windows.Forms.RichTextBox();
            this.gbReplace = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnReplaceOnce = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.lblConvert = new System.Windows.Forms.Label();
            this.txtReplace2 = new System.Windows.Forms.TextBox();
            this.btnReplace = new System.Windows.Forms.Button();
            this.gbMerge = new System.Windows.Forms.GroupBox();
            this.btnMerge = new System.Windows.Forms.Button();
            this.btnOpenM = new System.Windows.Forms.Button();
            this.txtMergePath = new System.Windows.Forms.TextBox();
            this.chkFooter = new System.Windows.Forms.CheckBox();
            this.btnExcelEditor = new System.Windows.Forms.Button();
            this.gbEdit.SuspendLayout();
            this.gbExport.SuspendLayout();
            this.gbReplace.SuspendLayout();
            this.gbMerge.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(12, 12);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(468, 26);
            this.txtPath.TabIndex = 0;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(486, 12);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(101, 28);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // cmbPage
            // 
            this.cmbPage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPage.FormattingEnabled = true;
            this.cmbPage.Location = new System.Drawing.Point(593, 12);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(95, 28);
            this.cmbPage.TabIndex = 2;
            this.cmbPage.SelectedIndexChanged += new System.EventHandler(this.cmbPage_SelectedIndexChanged);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveAs.Location = new System.Drawing.Point(123, 584);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(100, 31);
            this.btnSaveAs.TabIndex = 4;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(12, 584);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 31);
            this.button1.TabIndex = 5;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Location = new System.Drawing.Point(23, 85);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(107, 31);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnFont
            // 
            this.btnFont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFont.Location = new System.Drawing.Point(23, 149);
            this.btnFont.Name = "btnFont";
            this.btnFont.Size = new System.Drawing.Size(107, 31);
            this.btnFont.TabIndex = 8;
            this.btnFont.Text = "Font";
            this.btnFont.UseVisualStyleBackColor = true;
            this.btnFont.Click += new System.EventHandler(this.btnFont_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(191, 66);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(124, 32);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(23, 66);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(123, 32);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(25, 34);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(290, 26);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearchAndConvert_TextChanged);
            // 
            // gbEdit
            // 
            this.gbEdit.Controls.Add(this.btnNew);
            this.gbEdit.Controls.Add(this.btnPrint);
            this.gbEdit.Controls.Add(this.btnFont);
            this.gbEdit.Location = new System.Drawing.Point(713, 283);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Size = new System.Drawing.Size(177, 201);
            this.gbEdit.TabIndex = 10;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "Edit";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNew.Location = new System.Drawing.Point(23, 25);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(107, 31);
            this.btnNew.TabIndex = 6;
            this.btnNew.Text = "New Page";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // gbExport
            // 
            this.gbExport.Controls.Add(this.btnExcel);
            this.gbExport.Controls.Add(this.btnText);
            this.gbExport.Controls.Add(this.btnWord);
            this.gbExport.Location = new System.Drawing.Point(895, 283);
            this.gbExport.Name = "gbExport";
            this.gbExport.Size = new System.Drawing.Size(156, 201);
            this.gbExport.TabIndex = 11;
            this.gbExport.TabStop = false;
            this.gbExport.Text = "Export";
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcel.Location = new System.Drawing.Point(22, 149);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(107, 31);
            this.btnExcel.TabIndex = 9;
            this.btnExcel.Text = "Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnText
            // 
            this.btnText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnText.Location = new System.Drawing.Point(22, 85);
            this.btnText.Name = "btnText";
            this.btnText.Size = new System.Drawing.Size(107, 31);
            this.btnText.TabIndex = 8;
            this.btnText.Text = "Text";
            this.btnText.UseVisualStyleBackColor = true;
            this.btnText.Click += new System.EventHandler(this.btnText_Click);
            // 
            // btnWord
            // 
            this.btnWord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWord.Location = new System.Drawing.Point(22, 25);
            this.btnWord.Name = "btnWord";
            this.btnWord.Size = new System.Drawing.Size(107, 31);
            this.btnWord.TabIndex = 7;
            this.btnWord.Text = "Word";
            this.btnWord.UseVisualStyleBackColor = true;
            this.btnWord.Click += new System.EventHandler(this.btnWord_Click);
            // 
            // rtbContent
            // 
            this.rtbContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbContent.Location = new System.Drawing.Point(12, 46);
            this.rtbContent.Name = "rtbContent";
            this.rtbContent.Size = new System.Drawing.Size(695, 532);
            this.rtbContent.TabIndex = 3;
            this.rtbContent.Text = "";
            // 
            // gbReplace
            // 
            this.gbReplace.Controls.Add(this.btnNext);
            this.gbReplace.Controls.Add(this.btnReplaceOnce);
            this.gbReplace.Controls.Add(this.btnClear);
            this.gbReplace.Controls.Add(this.btnUndo);
            this.gbReplace.Controls.Add(this.btnSearch);
            this.gbReplace.Controls.Add(this.lblConvert);
            this.gbReplace.Controls.Add(this.txtSearch);
            this.gbReplace.Controls.Add(this.txtReplace2);
            this.gbReplace.Controls.Add(this.btnReplace);
            this.gbReplace.Location = new System.Drawing.Point(711, 12);
            this.gbReplace.Name = "gbReplace";
            this.gbReplace.Size = new System.Drawing.Size(340, 265);
            this.gbReplace.TabIndex = 12;
            this.gbReplace.TabStop = false;
            this.gbReplace.Text = "Search and Replace";
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNext.Location = new System.Drawing.Point(199, 156);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(116, 31);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnReplaceOnce
            // 
            this.btnReplaceOnce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReplaceOnce.Location = new System.Drawing.Point(25, 160);
            this.btnReplaceOnce.Name = "btnReplaceOnce";
            this.btnReplaceOnce.Size = new System.Drawing.Size(121, 31);
            this.btnReplaceOnce.TabIndex = 17;
            this.btnReplaceOnce.Text = "Replace Once";
            this.btnReplaceOnce.UseVisualStyleBackColor = true;
            this.btnReplaceOnce.Click += new System.EventHandler(this.btnReplaceOnce_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(201, 211);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(112, 32);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // lblConvert
            // 
            this.lblConvert.AutoSize = true;
            this.lblConvert.Location = new System.Drawing.Point(126, 101);
            this.lblConvert.Name = "lblConvert";
            this.lblConvert.Size = new System.Drawing.Size(82, 20);
            this.lblConvert.TabIndex = 4;
            this.lblConvert.Text = "Convert to";
            // 
            // txtReplace2
            // 
            this.txtReplace2.Location = new System.Drawing.Point(25, 124);
            this.txtReplace2.Name = "txtReplace2";
            this.txtReplace2.Size = new System.Drawing.Size(290, 26);
            this.txtReplace2.TabIndex = 3;
            this.txtReplace2.TextChanged += new System.EventHandler(this.txtSearchAndConvert_TextChanged);
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(25, 211);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(123, 32);
            this.btnReplace.TabIndex = 1;
            this.btnReplace.Text = "Replace All";
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // gbMerge
            // 
            this.gbMerge.Controls.Add(this.btnMerge);
            this.gbMerge.Controls.Add(this.btnOpenM);
            this.gbMerge.Controls.Add(this.txtMergePath);
            this.gbMerge.Location = new System.Drawing.Point(713, 490);
            this.gbMerge.Name = "gbMerge";
            this.gbMerge.Size = new System.Drawing.Size(339, 94);
            this.gbMerge.TabIndex = 13;
            this.gbMerge.TabStop = false;
            this.gbMerge.Text = "Merge PDF";
            // 
            // btnMerge
            // 
            this.btnMerge.Location = new System.Drawing.Point(232, 58);
            this.btnMerge.Name = "btnMerge";
            this.btnMerge.Size = new System.Drawing.Size(101, 28);
            this.btnMerge.TabIndex = 3;
            this.btnMerge.Text = "Merge";
            this.btnMerge.UseVisualStyleBackColor = true;
            this.btnMerge.Click += new System.EventHandler(this.btnMerge_Click);
            // 
            // btnOpenM
            // 
            this.btnOpenM.Location = new System.Drawing.Point(6, 58);
            this.btnOpenM.Name = "btnOpenM";
            this.btnOpenM.Size = new System.Drawing.Size(101, 28);
            this.btnOpenM.TabIndex = 2;
            this.btnOpenM.Text = "Open";
            this.btnOpenM.UseVisualStyleBackColor = true;
            this.btnOpenM.Click += new System.EventHandler(this.btnOpenM_Click);
            // 
            // txtMergePath
            // 
            this.txtMergePath.Location = new System.Drawing.Point(6, 26);
            this.txtMergePath.Name = "txtMergePath";
            this.txtMergePath.ReadOnly = true;
            this.txtMergePath.Size = new System.Drawing.Size(327, 26);
            this.txtMergePath.TabIndex = 0;
            // 
            // chkFooter
            // 
            this.chkFooter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkFooter.AutoSize = true;
            this.chkFooter.Location = new System.Drawing.Point(229, 591);
            this.chkFooter.Name = "chkFooter";
            this.chkFooter.Size = new System.Drawing.Size(115, 24);
            this.chkFooter.TabIndex = 14;
            this.chkFooter.Text = "Add Footer";
            this.chkFooter.UseVisualStyleBackColor = true;
            // 
            // btnExcelEditor
            // 
            this.btnExcelEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExcelEditor.Location = new System.Drawing.Point(350, 584);
            this.btnExcelEditor.Name = "btnExcelEditor";
            this.btnExcelEditor.Size = new System.Drawing.Size(130, 31);
            this.btnExcelEditor.TabIndex = 15;
            this.btnExcelEditor.Text = "Excel Editor";
            this.btnExcelEditor.UseVisualStyleBackColor = true;
            this.btnExcelEditor.Click += new System.EventHandler(this.ExcelEditor_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1061, 627);
            this.Controls.Add(this.btnExcelEditor);
            this.Controls.Add(this.chkFooter);
            this.Controls.Add(this.gbMerge);
            this.Controls.Add(this.gbReplace);
            this.Controls.Add(this.gbExport);
            this.Controls.Add(this.gbEdit);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.rtbContent);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.txtPath);
            this.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.Name = "Form1";
            this.Text = "PDF Editor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.gbEdit.ResumeLayout(false);
            this.gbExport.ResumeLayout(false);
            this.gbReplace.ResumeLayout(false);
            this.gbReplace.PerformLayout();
            this.gbMerge.ResumeLayout(false);
            this.gbMerge.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnFont;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.GroupBox gbExport;
        private System.Windows.Forms.Button btnWord;
        private System.Windows.Forms.Button btnText;
        private System.Windows.Forms.RichTextBox rtbContent;
        private System.Windows.Forms.GroupBox gbReplace;
        private System.Windows.Forms.Label lblConvert;
        private System.Windows.Forms.TextBox txtReplace2;
        private System.Windows.Forms.Button btnReplace;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.GroupBox gbMerge;
        private System.Windows.Forms.Button btnMerge;
        private System.Windows.Forms.Button btnOpenM;
        private System.Windows.Forms.TextBox txtMergePath;
        private System.Windows.Forms.CheckBox chkFooter;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnExcelEditor;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnReplaceOnce;
    }
}

